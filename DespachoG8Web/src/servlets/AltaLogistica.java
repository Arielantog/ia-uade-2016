package servlets;

import java.io.IOException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dto.LogisticaDTO;
import ejbModule.AdminWebLocal;

@WebServlet("/AltaLogistica")
public class AltaLogistica extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	AdminWebLocal admin;
	
    public AltaLogistica() {
        super();
        try {
			InitialContext context = new InitialContext();
			admin = (AdminWebLocal) context.lookup("java:global/DespachoG8EAR/DespachoG8//AdminWeb!ejbModule.AdminWebLocal");
		} catch (NamingException e) {
			e.printStackTrace();
		}
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Llamada a servlet AltaLogistica");
		String codigo = request.getParameter("codigo");
		String address = request.getParameter("address");
		String destination = request.getParameter("destination");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String syncro = request.getParameter("sync");
		String urlEventos = request.getParameter("urlEventos");
		String urlNotificacionDespacho = request.getParameter("urlNotificacionDespacho");
		String status = request.getParameter("estado");
		
		boolean estado = false;
		if (status.equals("1"))
			estado = true;
		else if (status.equals("2"))
			estado = false;
		boolean sync = false;
		if (syncro.equals("B"))
			sync = true;
		else if(syncro.equals("C"))
			sync = false;
		String valid = "Error al procesar.";
		String alert = "alert-info";
		if (!datosCompletos(codigo, address, destination, username, password, urlEventos, urlNotificacionDespacho))
			valid = "Faltan datos para crear log�stica.";
		else if (admin.getLogistica(codigo) != null)
		{
			valid = "Ya existe log�stica con el c�digo ingresado";
			alert = "alert-warning";
		}
		else
		{
			LogisticaDTO logistica = new LogisticaDTO();
			logistica.setIdLogistica(codigo);
			logistica.setAddress(address);
			logistica.setDestination(destination);
			logistica.setUsername(username);
			logistica.setPassword(password);
			logistica.setSync(sync);
			logistica.setUrlEventos(urlEventos);
			logistica.setUrlNotificacionDespacho(urlNotificacionDespacho);
			logistica.setEnabled(estado);
			if (admin.agregarLogistica(logistica) != -1)
			{
				valid = "Log�stica creada correctamente.";
				alert = "alert-success";
			}
			else
			{
				valid = "Error al crear log�stica.";
				alert = "alert-danger";
			}
		}
		request.setAttribute("valid2", valid);
		request.setAttribute("alert2", alert);
		request.getRequestDispatcher("/Logisticas").forward(request, response);
	}
	
	private boolean datosCompletos(String codigo, String address, String destination, String username, String password, String urlEventos, String urlNotificacionDespacho)
	{
		boolean res = true;
		if (codigo.equalsIgnoreCase(""))
			res = false;
		if (address.equalsIgnoreCase(""))
			res = false;
		if (destination.equalsIgnoreCase(""))
			res = false;
		if (username.equalsIgnoreCase(""))
			res = false;
		if (password.equalsIgnoreCase(""))
			res = false;
		if (urlEventos.equalsIgnoreCase(""))
			res = false;
		if (urlNotificacionDespacho.equalsIgnoreCase(""))
			res = false;
		
		return res;
	}

}
