package manager;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import dto.RecepcionStockDTO;
import ejbModule.Notificaciones;
import ejbModule.RecepcionStockLocal;
import ejbModule.RecepcionStockRemote;
import negocio.Articulo;
import negocio.ItemDespacho;
import negocio.OrdenDespacho;
import negocio.RecepcionStock;

@Stateless
@LocalBean
public class ManagerRecepcionStock implements RecepcionStockRemote, RecepcionStockLocal{
	
		
	@EJB
	private ManagerPersistencia managerPersistencia;
	@EJB private Notificaciones notificar;
	
	@Override
	public void recibirArticulo(RecepcionStockDTO recepcionDTO) {
		Logger.getAnonymousLogger().info("Entrando a Manager Recepcion Stock Recibir Stock");
		Articulo art = managerPersistencia.buscarArticulo(recepcionDTO.getArticulo().getCodigoArticulo());
    	RecepcionStock recepcion = new RecepcionStock(art, recepcionDTO.getCantidad());
    	int nroRecepcion = managerPersistencia.agregarRecepcionStock(recepcion);
    	notificar.notificarEventoLogistica(managerPersistencia.buscarLogisticaActivo(), "Recepcion de Stock para Articulo: " + recepcion.getArticulo().getCodigoArticulo() + ". Cantidad: " + recepcion.getCantidad() + ".");
    	Logger.getAnonymousLogger().info("Codigo Recepcion Stock: " + nroRecepcion);
		int cantidad = recepcion.getCantidad();
		while (cantidad > 0){
			OrdenDespacho ordenDespacho = managerPersistencia.buscarOrdenActualizar(recepcion.getArticulo().getCodigoArticulo());
			if (ordenDespacho == null)
			{
				Logger.getAnonymousLogger().info("Codigo Orden Despacho: No trajo ninguna");
				cantidad = 0;
			}	
			else
			{
				Logger.getAnonymousLogger().info("Codigo Orden Despacho: " + ordenDespacho.getCodigoOrdenDespacho());
				cantidad = actualizarItemDespacho(recepcion.getArticulo().getCodigoArticulo(), cantidad, ordenDespacho);
				actualizarOrdenDespacho(ordenDespacho);
			}
			
			
		}
	}

	private void actualizarOrdenDespacho(OrdenDespacho ordenDespacho) {
		Logger.getAnonymousLogger().info("Entrando a Manager Recepcion Stock Actualizar Orden de Despacho");
		boolean actualizar = true;
		for (ItemDespacho itemDespacho: ordenDespacho.getItemsDespacho()){
			if (itemDespacho.getEstado().equals("PENDIENTE"))
				actualizar = false;
		}
		if (actualizar){
			ordenDespacho.setEstado("LISTO");
			managerPersistencia.actualizarOrdenDespacho(ordenDespacho);
		}
	}

	private int actualizarItemDespacho(int codigoArticulo, int cantidad, OrdenDespacho ordenDespacho) {
		Logger.getAnonymousLogger().info("Entrando a Manager Recepcion Stock Actualizar Item de Orden de Despacho");
		List<ItemDespacho> itemsDespacho = ordenDespacho.getItemsDespacho();
		for(ItemDespacho itemDespacho: itemsDespacho)
			if(itemDespacho.getArticulo().getCodigoArticulo() == codigoArticulo){
				if(itemDespacho.getCantidadPendiente() <= cantidad)
				{
					cantidad = cantidad - itemDespacho.getCantidadPendiente();
					itemDespacho.setCantidadPendiente(0);
					itemDespacho.setEstado("COMPLETADO");
					managerPersistencia.actualizarItemDespacho(itemDespacho);
				}
				else
				{
					itemDespacho.setCantidadPendiente(itemDespacho.getCantidadPendiente() - cantidad);
					cantidad = 0;
					managerPersistencia.actualizarItemDespacho(itemDespacho);
				}
			}
		return cantidad;
	}


}
