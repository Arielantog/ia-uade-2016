package entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import negocio.PortalWeb;

@SuppressWarnings("serial")
@Entity
@Table (name="PortalesWeb")
public class PortalWebBean implements Serializable{
	@Id
	private String codigoPortal;
	private String address;
	private String urlNotificacionDespacho;
	private boolean enabled;

	public String getCodigoPortal() {
		return codigoPortal;
	}

	public void setCodigoPortal(String codigoPortal) {
		this.codigoPortal = codigoPortal;
	}

	public PortalWeb getNegocio() {
		PortalWeb portalWeb = new PortalWeb(codigoPortal);
		portalWeb.setAddress(this.address);
		portalWeb.setEnabled(this.enabled);
		portalWeb.setUrlNotificacionDespacho(this.urlNotificacionDespacho);
		return portalWeb;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUrlNotificacionDespacho() {
		return urlNotificacionDespacho;
	}

	public void setUrlNotificacionDespacho(String urlNotificacionDespacho) {
		this.urlNotificacionDespacho = urlNotificacionDespacho;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
}
