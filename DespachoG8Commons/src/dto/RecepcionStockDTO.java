package dto;

import java.io.Serializable;
import java.util.Date;


public class RecepcionStockDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private int codigoRecepcionStock;
	private ArticuloDTO articulo;
	private int cantidad;
	private Date fechaRecepcion;
	
	public int getCodigoRecepcionStock() {
		return codigoRecepcionStock;
	}
	public void setCodigoRecepcionStock(int codigoRecepcionStock) {
		this.codigoRecepcionStock = codigoRecepcionStock;
	}
	public ArticuloDTO getArticulo() {
		return articulo;
	}
	public void setArticulo(ArticuloDTO articulo) {
		this.articulo = articulo;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public Date getFechaRecepcion() {
		return fechaRecepcion;
	}
	public void setFechaRecepcion(Date fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}

}
