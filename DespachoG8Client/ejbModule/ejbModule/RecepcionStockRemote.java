package ejbModule;

import javax.ejb.Remote;

import dto.RecepcionStockDTO;

@Remote
public interface RecepcionStockRemote {
	public void recibirArticulo(RecepcionStockDTO recepcionDTO);
}
