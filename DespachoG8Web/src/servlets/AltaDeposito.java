package servlets;

import java.io.IOException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.DepositoDTO;
import ejbModule.AdminWebLocal;

/**
 * Servlet implementation class AltaDeposito
 */
@WebServlet("/AltaDeposito")
public class AltaDeposito extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	AdminWebLocal admin;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AltaDeposito() {
        super();
        try {
			InitialContext context = new InitialContext();
			admin = (AdminWebLocal) context.lookup("java:global/DespachoG8EAR/DespachoG8//AdminWeb!ejbModule.AdminWebLocal");
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Llamada a servlet AltaDeposito");
		String codigo = request.getParameter("codigo");
		String address = request.getParameter("address");
		String destination = request.getParameter("destination");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String status = request.getParameter("estado");
		boolean estado = false;
		if (status.equals("1"))
			estado = true;
		else if (status.equals("2"))
			estado = false;
		String valid = "Error al procesar.";
		String alert = "alert-info";
		if (!datosCompletos(codigo, address, destination, username, password, status))
			valid = "Faltan datos para crear el dep�sito.";
		else if (admin.getDeposito(codigo) != null)
		{
			valid = "Ya existe un dep�sito con el c�digo ingresado";
			alert = "alert-warning";
		}
		else
		{
			DepositoDTO depo = new DepositoDTO();
			depo.setAddress(address);
			depo.setCodigoDeposito(codigo);
			depo.setDestination(destination);
			depo.setEnabled(estado);
			depo.setPassword(password);
			depo.setUsername(username);
			if (admin.agregarDeposito(depo) != -1)
			{
				valid = "Deposito creado correctamente.";
				alert = "alert-success";
			}
			else
			{
				valid = "Error al crear deposito.";
				alert = "alert-danger";
			}
		}
		request.setAttribute("valid2", valid);
		request.setAttribute("alert2", alert);
		System.out.println("Info recolectada: Codigo = " + codigo + " - Address = " + address + " - Destination = " + destination + " - Username = " + username + " - Password = " + password + " - Habilitado = " + estado);
		request.getRequestDispatcher("/Depositos").forward(request, response);
	}
	
	private boolean datosCompletos(String codigo, String address, String destination, String username, String password, String status)
	{
		boolean res = true;
		if (codigo.equalsIgnoreCase(""))
			res = false;
		if (address.equalsIgnoreCase(""))
			res = false;
		if (destination.equalsIgnoreCase(""))
			res = false;
		if (username.equalsIgnoreCase(""))
			res = false;
		if (password.equalsIgnoreCase(""))
			res = false;
		if (status.equalsIgnoreCase(""))
			res = false;
		
		return res;
	}

}
