<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*" %>
<%@ page import="dto.ArticuloDTO" %>
<%@ page import="dto.DepositoDTO" %>
<!DOCTYPE HTML>
<!--
	GRUPO 8 
-->
<html>
	<head>
		<title>TP INTEGRACION</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="/DespachoWeb/assets/css/main.css" />
		<link rel="stylesheet" href="/DespachoWeb/assets/css/added.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body>

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Main -->
					<div id="main">
						<div class="inner">

							<!-- Header -->
								<header id="header">
									<a href="index.html" class="logo"><strong>Integracion de Aplicaciones</strong></a>
								
								</header>

							<!-- Banner -->
								<section id="banner">
									<div class="content">
										<header>
										<h1>Grupo 8<br /></h1>
										<p> Antognini - Burgos - De Martinis - Romanello</p>
										</header>
										<form action="ListarPorDeposito" method="get">
										<div class="row uniform">
											<h3>Dep�sito: <%=request.getAttribute("deposito")%></h3>
											<table>
										<tr>
										<th>Codigo de Art�culo</th>
										<th>Descripcion</th>
										</tr>
										<!-- cargo articulos del deposito -->
										<%
											if (request.getAttribute("articulos")!=null)
											{
												List<ArticuloDTO> articulos = (List<ArticuloDTO>)request.getAttribute("articulos");
												Iterator<ArticuloDTO> iart = articulos.iterator();
												while(iart.hasNext())
												{
													ArticuloDTO a = iart.next();
										%>
											<tr>
											<td><%= a.getCodigoArticulo() %></td>
											<td><%= a.getDescripcion() %></td>
											</tr>
										<%
												}
											}
										%> 
										</table>
										<!--  -->
											<div class="12u$">
											<ul class="actions">
											<li><a href="/DespachoWeb/ListarArticulos" class="button big">Volver</a></li>
											</ul>	
											</div>
										</div>
										</form>
										
										<div class="12u$">
										</div>
										</div>
									
									
									</div>
									</form>	
									
									
								</section>

						
						</div>
					</div>

				<!-- Sidebar -->
					<div id="sidebar">
						<div class="inner">

							<!-- Menu -->
								<nav id="menu">
									<header class="major">
										<h2>Menu</h2>
									</header>
									<ul>
											<li>
											<span class="opener">Usuario</span>
											<ul>

												<li><a href="/DespachoWeb/jsp/nuevoUsuario.jsp">Nuevo usuario</a></li>
												<li><a href="/DespachoWeb/ListarUsuarios">Listar usuarios</a></li>
												<li><a href="/DespachoWeb/jsp/adminUsuario.jsp">Administrar usuario</a></li>
											</ul>
											</li>
											<li>
											<span class="opener">Articulo/Solicitudes</span>
											<ul>
												<li><a href="/DespachoWeb/jsp/buscararticulo.jsp">Buscar articulo</a></li>
												<li><a href="/DespachoWeb/ListarArticulos">Listar articulo por filtro</a></li>
												<li><a href="/DespachoWeb/jsp/buscarsolicitud.jsp">Buscar solicitudes de articulos</a></li>
												<li><a href="/DespachoWeb/ListarSolicitudes">Listar solicitudes de articulos</a></li>												
											</ul>
										</li>
										<li>
											<span class="opener">Despacho</span>
											<ul>
												<li><a href="/DespachoWeb/jsp/buscarordendespacho.jsp">Buscar ordenes</a></li>
												<li><a href="/DespachoWeb/ListarOrdenDespacho">Listar ordenes</a></li>
											</ul>
										</li>
										<li>
											<span class="opener">M�dulos</span>
											<ul>
												<li><a href="/DespachoWeb/Depositos">Dep�sitos</a></li>
												<li><a href="/DespachoWeb/Portales">Portales Web</a></li>
												<li><a href="/DespachoWeb/Logisticas">Log�stica</a></li>
											</ul>
										</li>
																																				
									</ul>
								</nav>

							<!-- Section -->
								<section>
									<header class="major">
										<h2>P�gina principal</h2>
									</header>
								
									<ul class="actions">
										<li><a href="/DespachoWeb/index.html" class="button">Volver</a></li>
									</ul>
								</section>

						</div>
					</div>

			</div>

		<!-- Scripts -->
			<script src="/DespachoWeb/assets/js/jquery.min.js"></script>
			<script src="/DespachoWeb/assets/js/skel.min.js"></script>
			<script src="/DespachoWeb/assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="/DespachoWeb/assets/js/main.js"></script>

	</body>
</html>