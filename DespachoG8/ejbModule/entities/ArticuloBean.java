package entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import negocio.Articulo;

@SuppressWarnings("serial")
@Entity
@Table (name="Articulos")
public class ArticuloBean implements Serializable {
	@Id
	private int codigoArticulo;
	private String descripcion;
	
	public Integer getCodigoArticulo() {
		return codigoArticulo;
	}
	public void setCodigoArticulo(Integer codigoArticulo) {
		this.codigoArticulo = codigoArticulo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Articulo getNegocio() {
		Articulo articulo = new Articulo(this.codigoArticulo, this.descripcion);
		return articulo;
	}

}
