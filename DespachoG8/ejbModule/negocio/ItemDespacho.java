package negocio;

import dto.ItemDespachoDTO;
import entities.ItemDespachoBean;

public class ItemDespacho {
	private int codigoItemDespacho;
	private Articulo articulo;
	private int cantidad;
	private String estado;
	private int cantidadPendiente;
	
	public ItemDespacho(Articulo articulo, int cantidad) {
		this.articulo = articulo;
		this.cantidad = cantidad;
		this.estado = "PENDIENTE";
		this.cantidadPendiente = cantidad;
	}

	public int getCodigoItemDespacho() {
		return codigoItemDespacho;
	}

	public void setCodigoItemDespacho(int codigoItemDespacho) {
		this.codigoItemDespacho = codigoItemDespacho;
	}

	public Articulo getArticulo() {
		return articulo;
	}

	public void setArticulo(Articulo articulo) {
		this.articulo = articulo;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getCantidadPendiente() {
		return cantidadPendiente;
	}

	public void setCantidadPendiente(int cantidadPendiente) {
		this.cantidadPendiente = cantidadPendiente;
	}

	public ItemDespachoDTO getItemDespachoDTO() {
		ItemDespachoDTO itemDespachoDTO = new ItemDespachoDTO();
		itemDespachoDTO.setCodigoItemDespacho(codigoItemDespacho);
		itemDespachoDTO.setArticulo(articulo.getArticuloDTO());
		itemDespachoDTO.setCantidad(cantidad);
		itemDespachoDTO.setEstado(estado);
		return itemDespachoDTO;
	}

	public ItemDespachoBean getBean() {
		ItemDespachoBean itemDespachoBean = new ItemDespachoBean();
		itemDespachoBean.setCodigoItemDespacho(this.codigoItemDespacho);
		itemDespachoBean.setArticulo(this.articulo.getBean());
		itemDespachoBean.setCantidad(this.cantidad);
		itemDespachoBean.setEstado(this.estado);
		itemDespachoBean.setCantidadPendiente(this.cantidadPendiente);
		return itemDespachoBean;
	}
}
