package ejbModule;

import javax.ejb.Remote;

import dto.UsuarioDTO;

@Remote
public interface UsuarioRemote {
	
	public int agregarUsuario(String nombre, boolean estado);
	public UsuarioDTO buscarUsuario(int codigoUsuario);
	public void cambiarEstado(int codigoUsuario,boolean estado);
}
