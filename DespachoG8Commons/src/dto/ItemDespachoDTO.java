package dto;

import java.io.Serializable;

public class ItemDespachoDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private int codigoItemDespacho;
	private ArticuloDTO articulo;
	private int cantidad;
	private String estado;
	private int cantidadPendiente;
	
	
	public int getCodigoItemDespacho() {
		return codigoItemDespacho;
	}
	public void setCodigoItemDespacho(int codigoItemDespacho) {
		this.codigoItemDespacho = codigoItemDespacho;
	}
	public ArticuloDTO getArticulo() {
		return articulo;
	}
	public void setArticulo(ArticuloDTO articulo) {
		this.articulo = articulo;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public int getCantidadPendiente() {
		return cantidadPendiente;
	}
	public void setCantidadPendiente(int cantidadPendiente) {
		this.cantidadPendiente = cantidadPendiente;
	}
	
	
}
