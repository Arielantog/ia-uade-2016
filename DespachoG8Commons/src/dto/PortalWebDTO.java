package dto;

import java.io.Serializable;

public class PortalWebDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private String codigoPortalWeb;
	private String address;
	private String urlNotificacionDespacho;
	private boolean enabled;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUrlNotificacionDespacho() {
		return urlNotificacionDespacho;
	}

	public void setUrlNotificacionDespacho(String urlNotificacionDespacho) {
		this.urlNotificacionDespacho = urlNotificacionDespacho;
	}

	public PortalWebDTO() {
	}

	public String getCodigoPortalWeb() {
		return codigoPortalWeb;
	}

	public void setCodigoPortalWeb(String codigoPortalWeb) {
		this.codigoPortalWeb = codigoPortalWeb;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	
}
