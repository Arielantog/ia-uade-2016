package ejbModule;

import javax.ejb.Remote;

@Remote
public interface DepositoRemote {
	public void agregarDeposito(String codigoDeposito);
}
