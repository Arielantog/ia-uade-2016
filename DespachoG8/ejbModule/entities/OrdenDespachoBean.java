package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import negocio.ItemDespacho;
import negocio.OrdenDespacho;
import negocio.PortalWeb;

@SuppressWarnings("serial")
@Entity
@Table (name="OrdenesDespacho")
public class OrdenDespachoBean implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int codigoOrden;
	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="codigoOrdenDespacho")
	private Collection<ItemDespachoBean> itemsDespacho;
	private String estado;
	@ManyToOne
	@JoinColumn(name="codigoPortal")
	private PortalWebBean portalWeb;
	private Date fechaCreacion;
	private int idVenta;
	
	public int getCodigoOrden() {
		return codigoOrden;
	}
	public void setCodigoOrden(int codigoOrden) {
		this.codigoOrden = codigoOrden;
	}
	public Collection<ItemDespachoBean> getItemsDespacho() {
		return itemsDespacho;
	}
	public void setItemsDespacho(Collection<ItemDespachoBean> itemsDespacho) {
		this.itemsDespacho = itemsDespacho;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public PortalWebBean getPortalWeb() {
		return portalWeb;
	}
	public void setPortalWeb(PortalWebBean portalWeb) {
		this.portalWeb = portalWeb;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public int getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(int idVenta) {
		this.idVenta = idVenta;
	}
	
	public OrdenDespacho getNegocio(){
		PortalWeb portalWeb = this.portalWeb.getNegocio();
		
		OrdenDespacho ordenDespacho = new OrdenDespacho(portalWeb);
		ordenDespacho.setCodigoOrdenDespacho(this.codigoOrden);
		ordenDespacho.setEstado(estado);
		ordenDespacho.setFechaCreacion(this.fechaCreacion);
		ordenDespacho.setIdVenta(idVenta);
		
		List<ItemDespacho> itemsDespacho = new ArrayList<ItemDespacho>();
		for(ItemDespachoBean itemDespachoBean: this.itemsDespacho){
			ItemDespacho itemDespacho = new ItemDespacho(itemDespachoBean.getArticulo().getNegocio(),itemDespachoBean.getCantidad());
			itemDespacho.setCodigoItemDespacho(itemDespachoBean.getCodigoItemDespacho());
			itemDespacho.setEstado(itemDespachoBean.getEstado());
			itemDespacho.setCantidadPendiente(itemDespachoBean.getCantidadPendiente());
			
			itemsDespacho.add(itemDespacho);
		}
		
		ordenDespacho.setItemsDespacho(itemsDespacho);
		return ordenDespacho;
	}
	
}
