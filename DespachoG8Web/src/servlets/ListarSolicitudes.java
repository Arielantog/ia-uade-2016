package servlets;

import java.io.IOException;
import java.util.List;

import javax.naming.InitialContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.ArticuloDTO;
import dto.DepositoDTO;
import dto.SolicitudArticuloDTO;
import ejbModule.AdminWebLocal;

/**
 * Servlet implementation class ListarSolicitudes
 */
@WebServlet("/ListarSolicitudes")
public class ListarSolicitudes extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListarSolicitudes() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Carga para seleccion de deposito o articulo:
		try
		{
			InitialContext context = new InitialContext();
			AdminWebLocal manager = (AdminWebLocal) context.lookup("java:global/DespachoG8EAR/DespachoG8//AdminWeb!ejbModule.AdminWebLocal");
			if (manager.getListDepositos() != null && manager.getListDepositos().size() != 0)
			{
				System.out.println("existen depositos!");
				List<DepositoDTO> allDepositos = manager.getListDepositos();
				request.setAttribute("allDepositos", allDepositos);
			}
			
			if (manager.getArticulos() != null && manager.getArticulos().size() != 0)
			{
				System.out.println("tengo articulos");
				List<ArticuloDTO> allArticulos = manager.getArticulos();
				request.setAttribute("allArticulos", allArticulos);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		
		try
		{
			InitialContext context = new InitialContext();
			AdminWebLocal manager = (AdminWebLocal) context.lookup("java:global/DespachoG8EAR/DespachoG8//AdminWeb!ejbModule.AdminWebLocal");
			if (manager.getSolicitudesArticulos() != null && manager.getSolicitudesArticulos().size() != 0)
			{
				System.out.println("existen solicitudes");
				List<SolicitudArticuloDTO> solicitudes = manager.getSolicitudesArticulos();
				request.setAttribute("solicitudes", solicitudes);
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/listarsolicitudes.jsp");
				dispatcher.forward(request, response);
			}
			else
			{
				System.out.println("no hay solicitudes");
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/listarsolicitudes.jsp");
				dispatcher.forward(request, response);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
