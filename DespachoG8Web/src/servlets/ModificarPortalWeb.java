package servlets;

import java.io.IOException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dto.PortalWebDTO;
import ejbModule.AdminWebLocal;


@WebServlet("/ModificarPortalWeb")
public class ModificarPortalWeb extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
	AdminWebLocal admin;
	
    public ModificarPortalWeb() {
        super();
        try {
			InitialContext context = new InitialContext();
			admin = (AdminWebLocal) context.lookup("java:global/DespachoG8EAR/DespachoG8//AdminWeb!ejbModule.AdminWebLocal");
		} catch (NamingException e) {
			e.printStackTrace();
		}
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Llamada a servlet ModificarLogistica");
		
		String codigo = request.getParameter("codigo");
		String address = request.getParameter("address");
		String urlNotificacionDespacho = request.getParameter("urlNotificacionDespacho");
		String status = request.getParameter("estado");
		
		boolean estado = false;
		if (status != null)
			estado = true;
		String valid = "Error al procesar.";
		String alert = "alert-info";
		if (!datosCompletos(codigo, address, urlNotificacionDespacho))
			valid = "Faltan datos para modificar el dep�sito.";
		else if (admin.getPortalWeb(codigo) == null)
		{
			valid = "No se puede modificar el c�digo de Portal Web";
			alert = "alert-warning";
		}
		else
		{
			PortalWebDTO portalWeb = new PortalWebDTO();
			portalWeb.setCodigoPortalWeb(codigo);
			portalWeb.setAddress(address);
			portalWeb.setUrlNotificacionDespacho(urlNotificacionDespacho);
			portalWeb.setEnabled(estado);
			if (admin.modificarPortalWeb(portalWeb) != -1)
			{
				valid = "Portal Web modificado correctamente.";
				alert = "alert-success";
			}
			else
			{
				valid = "Error al modificar Portal Web.";
				alert = "alert-danger";
			}
		}
		request.setAttribute("valid", valid);
		request.setAttribute("alert", alert);
		request.getRequestDispatcher("/Portales").forward(request, response);
	}
	
	private boolean datosCompletos(String codigo, String address, String urlNotificacionDespacho)
	{
		boolean res = true;
		if (codigo.equalsIgnoreCase(""))
			res = false;
		if (address.equalsIgnoreCase(""))
			res = false;
		if (urlNotificacionDespacho.equalsIgnoreCase(""))
			res = false;
				
		return res;
	}

}
