<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*" %>
<%@ page import="dto.ArticuloDTO" %>
<%@ page import="dto.DepositoDTO" %>
<%@ page import="dto.SolicitudArticuloDTO" %>
<!DOCTYPE HTML>
<!--
	GRUPO 8 
-->
<html>
	<head>
		<title>TP INTEGRACION</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="/DespachoWeb/assets/css/main.css" />
		<link rel="stylesheet" href="/DespachoWeb/assets/css/added.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body>

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Main -->
					<div id="main">
						<div class="inner">

							<!-- Header -->
								<header id="header">
									<a href="index.html" class="logo"><strong>Integracion de Aplicaciones</strong></a>
								
								</header>

							<!-- Banner -->
								<section id="banner">
									<div class="content">
										<header>
										<h1>Grupo 8<br /></h1>
										<p> Antognini - Burgos - De Martinis - Romanello</p>
										</header>
										<form action="/DespachoWeb/ListarSolicitudes" method="post">
										<div class="row uniform">
											<h3>Solicitudes de Art�culos</h3>
											<div class="12u$">
												<table>
												<tr>
												<th>Codigo de Solicitud</th>
												<th>Fecha</th>
												<th>C�digo de Art�culo</th>
												<th>Art�culo</th>
												<th>Dep�sito</th>
												<th>Cantidad</th>
												</tr>
												
												<!--  cargo articulos en la tabla -->
												<%
													if (request.getAttribute("solicitudes")!=null)
													{
														List<SolicitudArticuloDTO> solicitudes = (List<SolicitudArticuloDTO>)request.getAttribute("solicitudes");
														Iterator<SolicitudArticuloDTO> isol = solicitudes.iterator();
														while(isol.hasNext())
														{
															SolicitudArticuloDTO s = isol.next();
												%>
														<tr>
															<td><%=s.getCodigoSolicitudArticulo() %></td>
															<td><%=s.getFechaSolicitud() %></td>
															<td><%=s.getArticulo().getCodigoArticulo() %></td>
															<td><%=s.getArticulo().getDescripcion() %></td>
															<td><%=s.getDeposito().getCodigoDeposito() %></td>
															<td><%=s.getCantidad() %></td>
													<% 	} %>
														</tr>
												<% } %>
												<!--  -->
												</table>
												</form>
											<div class="12u$">
											<form action="/DespachoWeb/ListarSolicitudFiltrada" method="post">
											<div class="select-wrapper">
											<select name="filtroDep" id="filtroDep">
											<option value="">- Filtro por Dep�sito: -</option>
											
											<!-- cargo depositos en el combo box -->
											<%
											if (request.getAttribute("allDepositos") != null)
											{
												List<DepositoDTO> allDepositos = (List<DepositoDTO>)request.getAttribute("allDepositos");
												Iterator<DepositoDTO> idepo = allDepositos.iterator();
												while(idepo.hasNext())
												{
													DepositoDTO dep = idepo.next();
											%>
													<option><%= dep.getCodigoDeposito() %></option>
											<%
												}
											}
											%>	
											<!--  -->
											
											</select>
											</div>
										
											<div><br><br></div>
										
											<div class="select-wrapper">
											<select name="filtroArt" id="filtroArt">
											<option value="">- Filtro por Art�culo: -</option>
											
											<!-- cargo articulos en el combo box -->
											<%
											if (request.getAttribute("allArticulos") != null)
											{
												List<ArticuloDTO> allArticulos = (List<ArticuloDTO>)request.getAttribute("allArticulos");
												Iterator<ArticuloDTO> iart = allArticulos.iterator();
												while(iart.hasNext())
												{
													ArticuloDTO art = iart.next();
											%>
													<option><%= art.getCodigoArticulo() %></option>
											<%
												}
											}
											%>	
											<!--  -->
											
											</select>
											</div>
										
											</div>
										<ul class="actions">
										<li><input type="submit" class="button big" value="Buscar" name="Buscar"></li>
										</ul>
										<div class="alert <%= request.getAttribute("alert") %>"><% String resp = (String) request.getAttribute("valid");
 																									if (resp != null && (!resp.isEmpty() || !resp.equalsIgnoreCase("null")))
																										out.println(resp); %></div> 
										</div>
										
										</div>
									</div>
									</form>	
									
									
								</section>

						
						</div>
					</div>

				<!-- Sidebar -->
					<div id="sidebar">
						<div class="inner">

							<!-- Menu -->
								<nav id="menu">
									<header class="major">
										<h2>Menu</h2>
									</header>
									<ul>
											<li>
											<span class="opener">Usuario</span>
											<ul>

												<li><a href="/DespachoWeb/jsp/nuevoUsuario.jsp">Nuevo usuario</a></li>
												<li><a href="/DespachoWeb/ListarUsuarios">Listar usuarios</a></li>
												<li><a href="/DespachoWeb/jsp/adminUsuario.jsp">Administrar usuario</a></li>
											</ul>
											</li>
											<li>
											<span class="opener">Articulo/Solicitudes</span>
											<ul>
												<li><a href="/DespachoWeb/jsp/buscararticulo.jsp">Buscar articulo</a></li>
												<li><a href="/DespachoWeb/ListarArticulos">Listar articulo por filtro</a></li>
												<li><a href="/DespachoWeb/jsp/buscarsolicitud.jsp">Buscar solicitudes de articulos</a></li>
												<li><a href="/DespachoWeb/ListarSolicitudes">Listar solicitudes de articulos</a></li>												
											</ul>
										</li>
										<li>
											<span class="opener">Despacho</span>
											<ul>
												<li><a href="/DespachoWeb/jsp/buscarordendespacho.jsp">Buscar ordenes</a></li>
												<li><a href="/DespachoWeb/ListarOrdenDespacho">Listar ordenes</a></li>
											</ul>
										</li>
										<li>
											<span class="opener">M�dulos</span>
											<ul>
												<li><a href="/DespachoWeb/Depositos">Dep�sitos</a></li>
												<li><a href="/DespachoWeb/Portales">Portales Web</a></li>
												<li><a href="/DespachoWeb/Logisticas">Log�stica</a></li>
											</ul>
										</li>
																																				
									</ul>
								</nav>

							<!-- Section -->
								<section>
									<header class="major">
										<h2>P�gina principal</h2>
									</header>
								
									<ul class="actions">
										<li><a href="/DespachoWeb/index.html" class="button">Volver</a></li>
									</ul>
								</section>

						</div>
					</div>

			</div>

		<!-- Scripts -->
			<script src="/DespachoWeb/assets/js/jquery.min.js"></script>
			<script src="/DespachoWeb/assets/js/skel.min.js"></script>
			<script src="/DespachoWeb/assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="/DespachoWeb/assets/js/main.js"></script>

	</body>
</html>