package servlets;

import java.io.IOException;
import java.util.List;

import javax.naming.InitialContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.OrdenDespachoDTO;
import ejbModule.AdminWebLocal;

/**
 * Servlet implementation class BuscarOrdenDespacho
 */
@WebServlet("/ListarOrdenDespacho")
public class ListarOrdenDespacho extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListarOrdenDespacho() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try
		{
			InitialContext context = new InitialContext();
			AdminWebLocal manager = (AdminWebLocal) context.lookup("java:global/DespachoG8EAR/DespachoG8//AdminWeb!ejbModule.AdminWebLocal");
			System.out.println("Comienzo servlet ListarOrdenDespacho");
			List<OrdenDespachoDTO> ordenesDespacho = manager.getOrdenesDespacho();
			if (ordenesDespacho != null)
				System.out.println("Se encontraron �rdenes");
			else
				System.out.println("No se encontraron �rdenes");
			request.setAttribute("ordenes", ordenesDespacho);
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/listarOrdenDespacho.jsp");
			dispatcher.forward(request, response);
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
		
		
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
