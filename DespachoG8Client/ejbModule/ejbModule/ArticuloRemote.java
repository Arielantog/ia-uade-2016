package ejbModule;

import javax.ejb.Remote;



@Remote
public interface ArticuloRemote {
	
	public void nuevoArticulo(int codigoArticulo, String descripcion, String codigoDeposito);
	
    
}
