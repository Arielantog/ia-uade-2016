package dto;

import java.io.Serializable;

public class LogisticaDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private String idLogistica;
	private String urlEventos;
	private String urlNotificacionDespacho;
	private String address;
	private String destination;
	private String username;
	private String password;
	private boolean sync;
	private boolean enabled;
	
	public LogisticaDTO() {
	}

	public String getIdLogistica() {
		return idLogistica;
	}

	public void setIdLogistica(String idLogistica) {
		this.idLogistica = idLogistica;
	}

	public String getUrlEventos() {
		return urlEventos;
	}

	public void setUrlEventos(String urlEventos) {
		this.urlEventos = urlEventos;
	}

	public String getUrlNotificacionDespacho() {
		return urlNotificacionDespacho;
	}

	public void setUrlNotificacionDespacho(String urlNotificacionDespacho) {
		this.urlNotificacionDespacho = urlNotificacionDespacho;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public boolean isSync() {
		return sync;
	}

	public void setSync(boolean sync) {
		this.sync = sync;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
