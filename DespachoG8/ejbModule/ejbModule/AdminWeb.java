package ejbModule;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import dto.ArticuloDTO;
import dto.DepositoDTO;
import dto.LogisticaDTO;
import dto.OrdenDespachoDTO;
import dto.PortalWebDTO;
import dto.SolicitudArticuloDTO;
import dto.UsuarioDTO;
import manager.ManagerPersistencia;
import negocio.Articulo;
import negocio.Deposito;
import negocio.Logistica;
import negocio.OrdenDespacho;
import negocio.PortalWeb;
import negocio.SolicitudArticulo;
import negocio.Usuario;

/**
 * Session Bean implementation class AdminWeb
 */
@Stateless
@LocalBean
public class AdminWeb implements AdminWebRemote, AdminWebLocal {

	
	@EJB
	 ManagerPersistencia managerPersistencia;
    /**
     * Default constructor. 
     */
    public AdminWeb() {
        // TODO Auto-generated constructor stub
    }
    
    // --- USUARIOS --- //
    public int agregarUsuario(UsuarioDTO usuarioDTO){
    	Logger.getAnonymousLogger().info("Entrando a AdminWeb Agregar Usuario");
    	Usuario usuario = new Usuario(usuarioDTO.getNombre(), usuarioDTO.isEstado());
		return managerPersistencia.agregarUsuario(usuario);
	}

	public UsuarioDTO getUsuario(int codigoUsuario) {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Get Usuario por Codigo");
		Usuario usuario = managerPersistencia.buscarUsuario(codigoUsuario);
		if (usuario != null)
			return usuario.getDTO();
		else
			return null;
	}
	
	public UsuarioDTO getUsuario(String nombre) {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Get Usuario por Nombre");
		Usuario usuario = managerPersistencia.buscarUsuario(nombre);
		if (usuario != null)
			return usuario.getDTO();
		else
			return null;
	}
	
	public List<UsuarioDTO> getListUsuarios() {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Listar Usuarios");
		List<UsuarioDTO> dtos = new ArrayList<UsuarioDTO>();
		List<Usuario> lista = managerPersistencia.getListUsuarios();
		if (lista != null)
		{
			for(Usuario obj: lista)
			{
				dtos.add(obj.getDTO());
			}
			return dtos;
		}
		else
			return null;
	}

	public int modificarUsuario(UsuarioDTO usuarioDTO) {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Modificar Usuario");
		try
		{
			Usuario usuario = new Usuario(usuarioDTO.getNombre(), usuarioDTO.isEstado());
			usuario.setCodigoUsuario(usuarioDTO.getCodigoUsuario());
			usuario.setEstado(usuarioDTO.isEstado());
			managerPersistencia.actualizarUsuario(usuario);
		}
		catch (Exception e)
		{
			Logger.getAnonymousLogger().info(e.getStackTrace().toString());
			return -1;
		}
			return 0;
	}

	public int eliminarUsuario(UsuarioDTO usuarioDTO) {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Eliminar Usuario");
		Usuario usuario = managerPersistencia.buscarUsuario(usuarioDTO.getCodigoUsuario());
		if (usuario != null)
			managerPersistencia.eliminarUsuario(usuario);
		return 0;
	}
	
	// --- PORTALES WEB --- //
	
	public int agregarPortalWeb(PortalWebDTO portalDTO) {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Agregar Portal Web");
		PortalWeb portal = new PortalWeb(portalDTO.getCodigoPortalWeb());
		portal.setAddress(portalDTO.getAddress());
		portal.setUrlNotificacionDespacho(portalDTO.getUrlNotificacionDespacho());
		portal.setEnabled(portalDTO.isEnabled());
		try{
		managerPersistencia.agregarPortalWeb(portal);
		}
		catch (Exception e)
		{
			System.out.println(e.getStackTrace());
			return -1;
		}
		return 0;
		
	}
	
	public int modificarPortalWeb(PortalWebDTO portalDTO) {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Modificar Portal Web");
		PortalWeb portal = new PortalWeb(portalDTO.getCodigoPortalWeb());
		portal.setAddress(portalDTO.getAddress());
		portal.setUrlNotificacionDespacho(portalDTO.getUrlNotificacionDespacho());
		portal.setEnabled(portalDTO.isEnabled());
		try{
		managerPersistencia.actualizarPortalWeb(portal);
		}
		catch (Exception e)
		{
			System.out.println(e.getStackTrace());
			return -1;
		}
		return 0;
	}
	
	public int eliminarPortalWeb(PortalWebDTO portalDTO) {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Eliminar Portal Web");
		try{
		PortalWeb portal = managerPersistencia.buscarPortalWeb(portalDTO.getCodigoPortalWeb());
		if (portal != null)
			managerPersistencia.eliminarPortalWeb(portal);
		}
		catch (Exception e)
		{
			System.out.println(e.getStackTrace());
			return -1;
		}
		return 0;
	}

	public PortalWebDTO getPortalWeb(String codigoPortal) {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Get Portal Web");
		PortalWeb portal = managerPersistencia.buscarPortalWeb(codigoPortal);
		if (portal != null)
			return portal.getDTO();
		else
			return null;
	}
	
	public List<PortalWebDTO> getListPortales() {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Listar Portales Web");
		List<PortalWebDTO> dtos = new ArrayList<PortalWebDTO>();
		List<PortalWeb> lista = managerPersistencia.getListPortalesWeb();
		if (lista != null)
		{
			for(PortalWeb obj: lista)
			{
				dtos.add(obj.getDTO());
			}
			return dtos;
		}
		else
			return null;
	}
	
	// --- DEPOSITOS --- //
	
	public int agregarDeposito(DepositoDTO depositoDTO){
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Agregar Deposito");
		Deposito deposito = new Deposito(depositoDTO.getCodigoDeposito());
		deposito.setDestination(depositoDTO.getDestination());
		deposito.setUsername(depositoDTO.getUsername());
		deposito.setPassword(depositoDTO.getPassword());
		deposito.setAddress(depositoDTO.getAddress());
		deposito.setEnabled(depositoDTO.isEnabled());
		try{
		managerPersistencia.agregarDeposito(deposito);
		}
		catch (Exception e)
		{
			System.out.println(e.getStackTrace());
			return -1;
		}
		return 0;
	}

	public int modificarDeposito(DepositoDTO depositoDTO) {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Modificar Deposito");
		Deposito deposito = new Deposito(depositoDTO.getCodigoDeposito());
		deposito.setDestination(depositoDTO.getDestination());
		deposito.setUsername(depositoDTO.getUsername());
		deposito.setPassword(depositoDTO.getPassword());
		deposito.setAddress(depositoDTO.getAddress());
		deposito.setEnabled(depositoDTO.isEnabled());
		try{
		managerPersistencia.actualizarDeposito(deposito);
		}
		catch (Exception e)
		{
			System.out.println(e.getStackTrace());
			return -1;
		}
		return 0;
	}

	public int eliminarDeposito(DepositoDTO depositoDTO) {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Eliminar Deposito");
		try{
		Deposito depo = managerPersistencia.buscarDeposito(depositoDTO.getCodigoDeposito());
		if(depo != null)
			managerPersistencia.eliminarDeposito(depo);
		}
		catch (Exception e)
		{
			System.out.println(e.getStackTrace());
			return -1;
		}
		return 0;
	}

	public DepositoDTO getDeposito(String codigoDeposito) {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Get Deposito");
		Deposito deposito = managerPersistencia.buscarDeposito(codigoDeposito);
		if(deposito != null)
			return deposito.getDTO();
		else
			return null;
	}
	
	public List<DepositoDTO> getListDepositos() {
		Logger.getAnonymousLogger().info("Entrando a AdmnWeb Listar Depositos");
		List<DepositoDTO> dtos = new ArrayList<DepositoDTO>();
		List<Deposito> lista = managerPersistencia.getListDepositos();
		if (lista != null)
		{
			for(Deposito obj: lista)
			{
				dtos.add(obj.getDTO());
			}
			return dtos;
		}
		else
			return null;
	}
	
	public List<ArticuloDTO> getArticulosPorDeposito(String codigoDeposito) {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Listar Articulos por Deposito");
		List<Articulo> articulos = managerPersistencia.buscarArticulosPorDeposito(codigoDeposito);
		List<ArticuloDTO> articulosDTO = new ArrayList<ArticuloDTO>();
		if (articulos != null)
		{
			for(Articulo articulo : articulos)
			{
				ArticuloDTO articuloDTO = articulo.getArticuloDTO();
				articulosDTO.add(articuloDTO);
			}
			return articulosDTO;
		}
		else
			return null;
	}
	
	// --- LOGISTICAS --- //

	public int agregarLogistica(LogisticaDTO logisticaDTO) {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Agregar Logistica");
		Logistica logistica = new Logistica(logisticaDTO.getIdLogistica(), logisticaDTO.getUrlEventos(), logisticaDTO.getUrlNotificacionDespacho());
		logistica.setAddress(logisticaDTO.getAddress());
		logistica.setEnabled(logisticaDTO.isEnabled());
		logistica.setSync(logisticaDTO.isSync());
		logistica.setDestination(logisticaDTO.getDestination());
		logistica.setUsername(logisticaDTO.getUsername());
		logistica.setPassword(logisticaDTO.getPassword());
		try{
		managerPersistencia.agregarLogistica(logistica);
		}catch (Exception e)
		{
			System.out.println(e.getStackTrace());
			return -1;
		}
		return 0;
	}

	public int modificarLogistica(LogisticaDTO logisticaDTO) {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Modificar Logistica");
		Logistica logistica = new Logistica(logisticaDTO.getIdLogistica(), logisticaDTO.getUrlEventos(), logisticaDTO.getUrlNotificacionDespacho());
		logistica.setAddress(logisticaDTO.getAddress());
		logistica.setEnabled(logisticaDTO.isEnabled());
		logistica.setSync(logisticaDTO.isSync());
		logistica.setDestination(logisticaDTO.getDestination());
		logistica.setUsername(logisticaDTO.getUsername());
		logistica.setPassword(logisticaDTO.getPassword());
		try{
		managerPersistencia.actualizarLogistica(logistica);
		}
		catch (Exception e)
		{
			System.out.println(e.getStackTrace());
			return -1;
		}
		return 0;
		
	}

	public int eliminarLogistica(LogisticaDTO logisticaDTO) {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Eliminar Logistica");
		try{
		Logistica logistica = managerPersistencia.buscarLogistica(logisticaDTO.getIdLogistica());
		if (logistica != null)
			managerPersistencia.eliminarLogistica(logistica);
		}
		catch (Exception e)
		{
			System.out.println(e.getStackTrace());
			return -1;
		}
		return 0;
	}

	public LogisticaDTO getLogistica(String idLogistica) {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Get Logistica");
		Logistica logistica = managerPersistencia.buscarLogistica(idLogistica);
		if (logistica != null)
			return logistica.getDTO();
		else
			return null;
	}

	public List<LogisticaDTO> getListLogisticas() {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Listar Logisticas");
		List<LogisticaDTO> dtos = new ArrayList<LogisticaDTO>();
		List<Logistica> lista = managerPersistencia.getListLogistica();
		if (lista != null)
		{
			for(Logistica obj: lista)
			{
				dtos.add(obj.getDTO());
			}
			return dtos;
		}
		else
			return null;
	}

	// --- ARTICULOS --- //
	public ArticuloDTO getArticulo(int codigoArticulo) {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Get Articulo");
		Articulo articulo = managerPersistencia.buscarArticulo(codigoArticulo);
		if (articulo != null)
			return articulo.getArticuloDTO();
		return null;
	}
	
	public DepositoDTO getDepositoArticulo(int codigoArticulo) {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Get Deposito con Articulo");
		DepositoDTO deposito = managerPersistencia.buscarDepositoConArticulo(codigoArticulo).getDTO();
		return deposito;
	}
	
	public List<ArticuloDTO> getArticulos() {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Listar Articulos");
		List<Articulo> articulos = managerPersistencia.buscarArticulos();
		if (articulos != null)
		{
			List<ArticuloDTO> articulosDTO = new ArrayList<ArticuloDTO>();
			for(Articulo articulo : articulos)
			{
				ArticuloDTO articuloDTO = articulo.getArticuloDTO();
				articulosDTO.add(articuloDTO);
			}
			return articulosDTO;
		}
		else
			return null;
	}
	
	// --- SOLICITUDES --- //
	
	public List<SolicitudArticuloDTO> getSolicitudesArticulos() {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Listar Solicitudes de Articulos");
		List<SolicitudArticulo> solicitudes = managerPersistencia.buscarSolicitudes();
		if (solicitudes != null)
		{
			List<SolicitudArticuloDTO> solicitudesDTO = new ArrayList<SolicitudArticuloDTO>();
			for(SolicitudArticulo solicitud : solicitudes)
			{
				SolicitudArticuloDTO solicitudDTO = solicitud.getSolicitudArticuloDTO();
				solicitudesDTO.add(solicitudDTO);
			}
			return solicitudesDTO;
		}
		else
			return null;
	}
		
	public SolicitudArticuloDTO getSolicitud(int nroSolicitud) {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Get Solicitud de Articulo");
		SolicitudArticulo solicitud = managerPersistencia.buscarSolicitud(nroSolicitud);
		if (solicitud != null)
			return solicitud.getSolicitudArticuloDTO();
		else
			return null;
	}
	
	public List<SolicitudArticuloDTO> getSolicitudesDepositoArticulo(String codigoDeposito, int codigoArticulo)
	{
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Get Solicitudes por Deposito y Articulo");
		List<SolicitudArticulo> solicitudes = managerPersistencia.buscarSolicitudesPorDepositoArticulo(codigoDeposito, codigoArticulo);
		if (solicitudes != null)
		{
			List<SolicitudArticuloDTO> solicitudesDTO = new ArrayList<SolicitudArticuloDTO>();
			for(SolicitudArticulo solicitud : solicitudes)
			{
				SolicitudArticuloDTO solicitudDTO = solicitud.getSolicitudArticuloDTO();
				solicitudesDTO.add(solicitudDTO);
			}
			return solicitudesDTO;
		}
		else
			return null;
	}
	
	public List<SolicitudArticuloDTO> getSolicitudesDeposito(String codigoDeposito)
	{
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Get Solicitudes por Deposito");
		List<SolicitudArticulo> solicitudes = managerPersistencia.buscarSolicitudesPorDeposito(codigoDeposito);
		if (solicitudes != null)
		{
			List<SolicitudArticuloDTO> solicitudesDTO = new ArrayList<SolicitudArticuloDTO>();
			for(SolicitudArticulo solicitud : solicitudes)
			{
				SolicitudArticuloDTO solicitudDTO = solicitud.getSolicitudArticuloDTO();
				solicitudesDTO.add(solicitudDTO);
			}
			return solicitudesDTO;
		}
		else
			return null;
	}

	public List<SolicitudArticuloDTO> getSolicitudesArticulo(int codigoArticulo)
	{
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Get Solicitudes por Articulo");
		List<SolicitudArticulo> solicitudes = managerPersistencia.buscarSolicitudesPorArticulo(codigoArticulo);
		if (solicitudes != null)
		{
			List<SolicitudArticuloDTO> solicitudesDTO = new ArrayList<SolicitudArticuloDTO>();
			for(SolicitudArticulo solicitud : solicitudes)
			{
				SolicitudArticuloDTO solicitudDTO = solicitud.getSolicitudArticuloDTO();
				solicitudesDTO.add(solicitudDTO);
			}
			return solicitudesDTO;
		}
		else
			return null;
	}
	
	// --- ORDENES DE DESPACHO --- //
	
	public OrdenDespachoDTO getOrdenDespacho(int codigoOrden) {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Get Orden de Despacho");
		OrdenDespacho orden = managerPersistencia.buscarOrdenDespacho(codigoOrden);
		if (orden != null)
			return orden.getOrdenDespachoDTO();
		else
			return null;
	}

	@Override
	public List<OrdenDespachoDTO> getOrdenesDespacho() {
		Logger.getAnonymousLogger().info("Entrando a AdminWeb Listar Ordenes de Despacho");
		List<OrdenDespachoDTO> ordenesDespacho = new ArrayList<OrdenDespachoDTO>(); 
		ordenesDespacho = managerPersistencia.getListOrdenes();
		return ordenesDespacho;
	}
}
