package servlets;

import java.io.IOException;
import java.util.List;

import javax.naming.InitialContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dto.UsuarioDTO;
import ejbModule.AdminWebLocal;

/**
 * Servlet implementation class ListarSolicitudes
 */
@WebServlet("/ListarUsuarios")
public class ListarUsuarios extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListarUsuarios() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("HOla");
		try
		{
			InitialContext context = new InitialContext();
			AdminWebLocal manager = (AdminWebLocal) context.lookup("java:global/DespachoG8EAR/DespachoG8//AdminWeb!ejbModule.AdminWebLocal");
			if (manager.getListUsuarios() != null && manager.getListUsuarios().size() != 0)
			{
				System.out.println("existen usuarios");
				List<UsuarioDTO> usuarios = manager.getListUsuarios();
				request.setAttribute("usuarios", usuarios);
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/listarUsuarios.jsp");
				dispatcher.forward(request, response);
			}
			else
			{
				System.out.println("no hay usuarios");
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/listarUsuarios.jsp");
				dispatcher.forward(request, response);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
