package ejbModule;

import java.util.List;

import javax.ejb.Local;

import dto.ArticuloDTO;
import dto.DepositoDTO;
import dto.LogisticaDTO;
import dto.OrdenDespachoDTO;
import dto.PortalWebDTO;
import dto.SolicitudArticuloDTO;
import dto.UsuarioDTO;

@Local
public interface AdminWebLocal {
	
	// --- USUARIOS --- //
	public int agregarUsuario(UsuarioDTO usuarioDTO);
	public int modificarUsuario(UsuarioDTO usuarioDTO);
	public int eliminarUsuario(UsuarioDTO usuarioDTO);
	public UsuarioDTO getUsuario(int codigoUsuario);
	public UsuarioDTO getUsuario(String nombre);
	public List<UsuarioDTO> getListUsuarios();
	
	// --- PORTALES WEB --- //
	public int agregarPortalWeb(PortalWebDTO portalDTO);
	public int modificarPortalWeb(PortalWebDTO portalDTO);
	public int eliminarPortalWeb(PortalWebDTO portalDTO);
	public PortalWebDTO getPortalWeb(String codigoPortal);
	public List<PortalWebDTO> getListPortales();
	
	// --- DEPOSITOS --- //	
	public int agregarDeposito(DepositoDTO depositoDTO);
	public int modificarDeposito(DepositoDTO depositoDTO);
	public int eliminarDeposito(DepositoDTO depositoDTO);
	public DepositoDTO getDeposito(String codigoDeposito);
	public List<DepositoDTO> getListDepositos();
	public List<ArticuloDTO> getArticulosPorDeposito(String codigoDeposito);
	
	// --- LOGISTICAS --- //
	public int agregarLogistica(LogisticaDTO logisticaDTO);
	public int modificarLogistica(LogisticaDTO logisticaDTO);
	public int eliminarLogistica(LogisticaDTO logisticaDTO);
	public LogisticaDTO getLogistica(String idLogistica);
	public List<LogisticaDTO> getListLogisticas();
	
	// --- ARTICULOS --- //
	public ArticuloDTO getArticulo(int codigoArticulo);
	public DepositoDTO getDepositoArticulo(int codigoArticulo);
	public List<ArticuloDTO> getArticulos();
	
	// --- SOLICITUDES --- //
	public List<SolicitudArticuloDTO> getSolicitudesArticulos();
	public SolicitudArticuloDTO getSolicitud(int nroSolicitud);
	public List<SolicitudArticuloDTO> getSolicitudesDepositoArticulo(String codigoDeposito, int codigoArticulo);
	public List<SolicitudArticuloDTO> getSolicitudesDeposito(String codigoDeposito);
	public List<SolicitudArticuloDTO> getSolicitudesArticulo(int codigoArticulo);
	
	// --- ORDENES DE DESPACHO --- //
	public OrdenDespachoDTO getOrdenDespacho(int codigoOrden);
	public List<OrdenDespachoDTO> getOrdenesDespacho();
}
