package RSOrdenDespacho;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Path("/test")
public class RSTest2 {
	
	@GET
	@Path("/saludo/{nombre}")
	@Produces({ "text/plain" })
	 public String lelele( @DefaultValue("Desconocido") @PathParam("nombre") String nombre) {
		return "Hola " + nombre + "!";
	 }

}
