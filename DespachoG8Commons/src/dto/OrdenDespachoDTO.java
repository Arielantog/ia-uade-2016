package dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class OrdenDespachoDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private int codigoOrdenDespacho;
	private List<ItemDespachoDTO> itemsDespacho;
	private String estado;
	private PortalWebDTO portalWeb;
	private int idVenta;
	private Date fechaCreacion;
	
	
	public int getCodigoOrdenDespacho() {
		return codigoOrdenDespacho;
	}
	public void setCodigoOrdenDespacho(int codigoOrdenDespacho) {
		this.codigoOrdenDespacho = codigoOrdenDespacho;
	}
	public List<ItemDespachoDTO> getItemsDespacho() {
		return itemsDespacho;
	}
	public void setItemsDespacho(List<ItemDespachoDTO> itemsDespacho) {
		this.itemsDespacho = itemsDespacho;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public PortalWebDTO getPortalWeb() {
		return portalWeb;
	}
	public void setPortalWeb(PortalWebDTO portalWeb) {
		this.portalWeb = portalWeb;
	}
	public int getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(int idVenta) {
		this.idVenta = idVenta;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
}
