package RSOrdenDespacho;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;

import dto.ArticuloDTO;
import dto.ItemDespachoDTO;
import dto.OrdenDespachoDTO;
import dto.PortalWebDTO;

public class RSTest {

	public static void main(String[] args) 
	{
		try {
			URL url = new URL("http://localhost:8080/DespachoG8Web/rest/service/ordenDespacho");
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setDoOutput(true);
			urlConnection.setRequestMethod("POST");
			urlConnection.setRequestProperty("Content-Type", "application/json");
				
			//Creo una OrdenDespachoDTO, le asigno ItemsDTO
			PortalWebDTO portal = new PortalWebDTO();
			portal.setCodigoPortalWeb("G01");
			ArticuloDTO art1 = new ArticuloDTO();
			art1.setCodigoArticulo(1);
			art1.setDescripcion("art 1"); //no tienen tambien deposito?
			ArticuloDTO art2 = new ArticuloDTO();
			art2.setCodigoArticulo(2);
			art2.setDescripcion("art 2");
			//La orden lleva solo: nro portal, items: codigo articulo + cantidad
			OrdenDespachoDTO orden = new OrdenDespachoDTO();
			orden.setPortalWeb(portal);
			List<ItemDespachoDTO> items = new ArrayList<ItemDespachoDTO>();
			ItemDespachoDTO item1 = new ItemDespachoDTO();
			item1.setArticulo(art1);
			item1.setCantidad(5);
			ItemDespachoDTO item2 = new ItemDespachoDTO();
			item2.setArticulo(art2);
			item2.setCantidad(2);
			items.add(item1);
			items.add(item2);
			orden.setItemsDespacho(items);
				
			//Paso la orden a json:
			String ordenJson = convertirJson(orden);
			IOUtils.write(ordenJson,urlConnection.getOutputStream());
			if (urlConnection.getResponseCode() != 200)
				throw new RuntimeException("Error de conexion: " + urlConnection.getResponseCode());
		
			String response = IOUtils.toString(urlConnection.getInputStream());
			System.out.println("Respuesta: " + response);
		}
		catch (Exception e)
		{
			e.getStackTrace();
			System.out.println(e.getMessage());
		}
	}

	public static String convertirJson(OrdenDespachoDTO orden)
	{
		Gson gson = new Gson();
		//Convierto a json:
		String json = gson.toJson(orden);
		System.out.println("Queda: " + json);
		return json; 
	}
}
