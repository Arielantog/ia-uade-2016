package negocio;

import java.util.Date;

import dto.SolicitudArticuloDTO;

public class SolicitudArticulo {
	private int codigoSolicitudArticulo;
	private Deposito deposito;
	private Articulo articulo;
	private int cantidad;
	private Date fechaSolicitud;
	
	public SolicitudArticulo(Deposito deposito, Articulo articulo, int cantidad) {
		super();
		this.deposito = deposito;
		this.articulo = articulo;
		this.cantidad = cantidad;
		this.fechaSolicitud = new Date();
	}
	
	public int getCodigoSolicitudArticulo() {
		return codigoSolicitudArticulo;
	}
	public void setCodigoSolicitudArticulo(int codigoSolicitudArticulo) {
		this.codigoSolicitudArticulo = codigoSolicitudArticulo;
	}
	public Deposito getDeposito() {
		return deposito;
	}
	public void setDeposito(Deposito deposito) {
		this.deposito = deposito;
	}
	public Articulo getArticulo() {
		return articulo;
	}
	public void setArticulo(Articulo articulo) {
		this.articulo = articulo;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	/*public SolicitudArticuloView getSolicitudArticuloView() {
		SolicitudArticuloView solicitudArticuloView = new SolicitudArticuloView();
		solicitudArticuloView.setCodigoSolicitudArticulo(codigoSolicitudArticulo);
		solicitudArticuloView.setDeposito(deposito.getDepositoView());
		solicitudArticuloView.setArticulo(articulo.getArticuloView());
		solicitudArticuloView.setCantidad(cantidad);
		return null;
	}*/
	
	public SolicitudArticuloDTO getSolicitudArticuloDTO(){
		SolicitudArticuloDTO solicitud = new SolicitudArticuloDTO();
		solicitud.setArticulo(this.articulo.getArticuloDTO());
		solicitud.setCantidad(this.cantidad);
		solicitud.setCodigoSolicitudArticulo(this.codigoSolicitudArticulo);
		solicitud.setDeposito(this.deposito.getDTO());
		solicitud.setFechaSolicitud(this.fechaSolicitud);
		return solicitud;
	}
}

