package servlets;

import java.io.IOException;
import java.util.List;

import javax.naming.InitialContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.ArticuloDTO;
import ejbModule.AdminWebLocal;

/**
 * Servlet implementation class ListarPorDeposito
 */
@WebServlet("/ListarPorDeposito")
public class ListarPorDeposito extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListarPorDeposito() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String alert = "alert-info";
		String valid = "Error";
		String sCodigoDeposito = request.getParameter("filtro");
		if (!sCodigoDeposito.equals(""))  
		{
			System.out.println("tengo un nro de deposito!");
			request.setAttribute("deposito", sCodigoDeposito);
			try
			{
				InitialContext context = new InitialContext();
				AdminWebLocal manager = (AdminWebLocal) context.lookup("java:global/DespachoG8EAR/DespachoG8//AdminWeb!ejbModule.AdminWebLocal");
				if (manager.getArticulosPorDeposito(sCodigoDeposito) != null && manager.getArticulosPorDeposito(sCodigoDeposito).size() != 0)
				{
					System.out.println("tengo articulos en ese deposito");
					List<ArticuloDTO> articulosPorDeposito = manager.getArticulosPorDeposito(sCodigoDeposito);
					request.setAttribute("articulos", articulosPorDeposito);
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/filtroarticulosdeposito.jsp");
					dispatcher.forward(request, response);
				}
				else
				{
					System.out.println("no hay articulos");
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/filtroarticulosdeposito.jsp");
					dispatcher.forward(request, response);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				System.out.println(e.getMessage());
			}
		}
		else
		{
			System.out.println("no hay deposito seleccionado!");
			valid = "Seleccionar un dep�sito.";
			alert = "alert-warning";
			request.setAttribute("valid", valid);
			request.setAttribute("alert", alert);
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ListarArticulos");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
