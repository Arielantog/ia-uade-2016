package servlets;

import java.io.IOException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.UsuarioDTO;
import ejbModule.AdminWebLocal;

/**
 * Servlet implementation class AltaUsuario
 */
@WebServlet("/AltaUsuario")
public class AltaUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	AdminWebLocal admin;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AltaUsuario() {
        super();
        try {
			InitialContext context = new InitialContext();
			admin = (AdminWebLocal) context.lookup("java:global/DespachoG8EAR/DespachoG8//AdminWeb!ejbModule.AdminWebLocal");
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		System.out.println("Llamada a servlet AltaUsuario");
		String usuario = request.getParameter("nombre");
		String status = request.getParameter("estado");
		String alert = "alert-info";
		boolean estado = false;
		if (status.equals("1"))
			estado = true;
		else if (status.equals("2"))
			estado = false;
		String valid = "Error al procesar.";
		// Verificar si los campos estan completos
		System.out.println("Info recolectada: Nombre = " + usuario + " - Numero Estado = " + status);
		if (usuario == null || usuario.equalsIgnoreCase("") || status == null || status.equalsIgnoreCase("0"))
			valid = "Faltan datos para cargar el usuario.";
		else if (admin.getUsuario(usuario) != null)
		{
			valid = "Ya existe el usuario que intenta crear.";
			alert = "alert-warning";
		}
		else
		{
			UsuarioDTO user = new UsuarioDTO();
			user.setEstado(estado);
			user.setNombre(usuario);
			if (admin.agregarUsuario(user) != -1)
			{
				valid = "Usuario creado correctamente.";
				alert = "alert-success";
			}
			else
			{
				valid = "Error al crear el usuario.";
				alert = "alert-danger";
			}
		}
		request.setAttribute("valid", valid);
		request.setAttribute("alert", alert);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/jsp/nuevoUsuario.jsp");
		dispatcher.forward(request, response);
		
	}

}
