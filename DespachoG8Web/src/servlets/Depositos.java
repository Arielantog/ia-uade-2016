package servlets;

import java.io.IOException;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.DepositoDTO;
import ejbModule.AdminWebLocal;

/**
 * Servlet implementation class Depositos
 */
@WebServlet("/Depositos")
public class Depositos extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	AdminWebLocal admin;
	
	//AdminWebLocal admin;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Depositos() {
        super();
        try {
			InitialContext context = new InitialContext();
			admin = (AdminWebLocal) context.lookup("java:global/DespachoG8EAR/DespachoG8//AdminWeb!ejbModule.AdminWebLocal");
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<DepositoDTO> depositos = admin.getListDepositos();
		request.setAttribute("depositos", depositos);
		request.getRequestDispatcher("/jsp/depositos.jsp").forward(request, response);
		
	}

}
