package cosasQueVienenDeAfuera;

public class EstadoPortal {
	
	private int numeroVenta;
	private String estado;
	
	public int getnumeroVenta() {
		return numeroVenta;
	}
	public void setnumeroVenta(int numeroVenta) {
		this.numeroVenta = numeroVenta;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

}
