package timed;

import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import ejbModule.Notificaciones;
import manager.ManagerPersistencia;
import negocio.Logistica;
import negocio.OrdenDespacho;

/**
 * Session Bean implementation class InformadorOrdenes
 */
@Singleton
@LocalBean
@Startup
public class InformadorOrdenes {

	private @Resource TimerService timerService;
	
	@EJB
	private ManagerPersistencia managerPersistencia;
	@EJB private Notificaciones notificar;
    /**
     * Default constructor. 
     */
    public InformadorOrdenes() {
        // TODO Auto-generated constructor stub
    }
    
    @PostConstruct
    public void createTimer()
    {
    	System.out.println("Create timer called...");
    	//timerService.createTimer(new Date().getTime(), "Timer Informar Ordenes de Despacho");
    	TimerConfig config = new TimerConfig();
    	config.setInfo("Timer Informar Ordenes");
    	timerService.createSingleActionTimer(30000, config);
    }
    
    //@Schedule(second="*/5", minute = "*", hour = "*", persistent = false)
    @Timeout
    public void informar(Timer timer)
    {
    	System.out.println("---------------------");
        System.out.println("Received Timer event: " + timer.getInfo());
        System.out.println("---------------------");
        
        this.informarOrdenesPortalWeb();
		this.informarOrdenesLogistica();
        
		timer.cancel();
        this.createTimer();
    }
    
    private void informarOrdenesLogistica() {
		Logger.getAnonymousLogger().info("Entrando a Manager Recepcion Stock Informar Ordenes a Logistica");
		List<OrdenDespacho> ordenesDespacho = managerPersistencia.buscarOrdenesLogistica();
		for (OrdenDespacho ordenDespacho: ordenesDespacho){
			boolean enviado = false;
			enviado = informarLogistica(ordenDespacho);
			
			if (enviado)
			{
				ordenDespacho.setEstado("ENTREGADA");
				managerPersistencia.actualizarOrdenDespacho(ordenDespacho);
			}
				
		}
	}

	private boolean informarLogistica(OrdenDespacho ordenDespacho) {
		Logger.getAnonymousLogger().info("Entrando a Manager Recepcion Stock Informar una Orden a Logistica");
		Logistica logistica = managerPersistencia.buscarLogisticaActivo();
		return notificar.notificarEntregaLogistica(logistica, ordenDespacho);
	}
	
	private void informarOrdenesPortalWeb() {
		Logger.getAnonymousLogger().info("Entrando a Manager Recepcion Stock Informar Ordenes a Portales Web");
		List<OrdenDespacho> ordenesDespacho = managerPersistencia.buscarOrdenesPortal();
		for (OrdenDespacho ordenDespacho: ordenesDespacho){
			boolean enviado = false;
			enviado = informarPortal(ordenDespacho);
			
			if (enviado){
				ordenDespacho.setEstado("ENTREGADA PORTAL");
				managerPersistencia.actualizarOrdenDespacho(ordenDespacho);
			}
				
		}
	}
	
	private boolean informarPortal(OrdenDespacho ordenDespacho) {
		Logger.getAnonymousLogger().info("Entrando a Manager Recepcion Stock Informar una Orden a Portal Web");
		return notificar.notificarEntregaPortalWeb(ordenDespacho);
	}

}
