package ejbModule;

import javax.ejb.Remote;

@Remote
public interface OrdenDespachoRemote {
	public int nuevaOrdenDespacho(String codigoPortalWeb);
	public void agregarItem(int codigoOrdenDespacho, int codigoArticulo, int cantidad) throws Exception;

}
