package servlets;

import java.io.IOException;

import javax.naming.InitialContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.SolicitudArticuloDTO;
import ejbModule.AdminWebLocal;

/**
 * Servlet implementation class BuscarSolicitud
 */
@WebServlet("/BuscarSolicitud")
public class BuscarSolicitud extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuscarSolicitud() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String alert = "alert-info";
		String valid = "Error";
		String sCodigoSolicitud = request.getParameter("codigo");
		if (!sCodigoSolicitud.equals(null) && !sCodigoSolicitud.equals(""))
		{
			System.out.println("Tengo codigo de solicitud");
			try
			{
				InitialContext context = new InitialContext();
				AdminWebLocal manager = (AdminWebLocal) context.lookup("java:global/DespachoG8EAR/DespachoG8//AdminWeb!ejbModule.AdminWebLocal");
				if (manager.getSolicitud(Integer.parseInt(sCodigoSolicitud))!=null)
				{
					System.out.println("existe la solicitud");
					SolicitudArticuloDTO solicitud = manager.getSolicitud(Integer.parseInt(sCodigoSolicitud));
					request.setAttribute("solicitud", solicitud);
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/buscarsolicitud.jsp");
					dispatcher.forward(request, response);
				}
				else
				{
					System.out.println("no existe la solicitud");
					valid = "El c�digo de solicitud de art�culo ingresado no existe.";
					alert = "alert-warning";
					request.setAttribute("valid", valid);
					request.setAttribute("alert", alert);
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/buscarsolicitud.jsp");
					dispatcher.forward(request, response);
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println(e.getMessage());
			}
		}
		else
		{
			System.out.println("no tengo un codigo de solicitud");
			valid = "Ingresar c�digo de solicitud de art�culo.";
			alert = "alert-warning";
			request.setAttribute("valid", valid);
			request.setAttribute("alert", alert);
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/buscarsolicitud.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
