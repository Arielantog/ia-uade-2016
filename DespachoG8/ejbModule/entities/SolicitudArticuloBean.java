package entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table (name="SolicitudesArticulos")
public class SolicitudArticuloBean implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int codigoSolicitudArticulo;
	@OneToOne
	@JoinColumn(name="codigoDeposito")
	private DepositoBean deposito;
	@OneToOne
	@JoinColumn(name="codigoArticulo")
	private ArticuloBean articulo;
	private int cantidad;
	private Date fechaSolicitud;
	
	
	public int getCodigoSolicitudArticulo() {
		return codigoSolicitudArticulo;
	}
	public void setCodigoSolicitudArticulo(int codigoSolicitudArticulo) {
		this.codigoSolicitudArticulo = codigoSolicitudArticulo;
	}
	public DepositoBean getDeposito() {
		return deposito;
	}
	public void setDeposito(DepositoBean deposito) {
		this.deposito = deposito;
	}
	public ArticuloBean getArticulo() {
		return articulo;
	}
	public void setArticulo(ArticuloBean articulo) {
		this.articulo = articulo;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}
	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}
	
}
