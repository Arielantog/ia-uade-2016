<!DOCTYPE HTML>
<!--
	GRUPO 8 
-->
<%@ page import="java.util.*,java.text.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<title>TP INTEGRACION</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="/DespachoWeb/assets/css/main.css" />
		<link rel="stylesheet" href="/DespachoWeb/assets/css/added.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script>
		$(document).ready(function(){
		    $("#toggler").click(function(){
		        $("#toggled").toggle();
		    });
		});
		</script>
	</head>
	<body>

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Main -->
					<div id="main">
						<div class="inner">

							<!-- Header -->
								<header id="header">
									<a href="index.html" class="logo"><strong>Integracion de Aplicaciones</strong></a>
								
								</header>

							<!-- Banner -->
								<section id="banner">
									<div class="content">
										<header>
											<h1>Grupo 8<br /></h1>
											<p> Antognini - Burgos - De Martinis - Romanello</p>
										</header>
										<p>Despacho</p>
									<h3>Dep�sitos</h3>
									<div class="row unifrom">
										<table class="table">
											<thead>
												<tr>
													<th>C�digo</th>
													<th>Address</th>
													<th>JMS Destination</th>
													<th>Username</th>
													<th>Password</th>
													<th>Enabled?</th>	
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${depositos}" var="depositos">
												    <tr>
												    	<form action="/DespachoWeb/ModificarDeposito" method="post">
													        <td><p>${depositos.codigoDeposito}</p><input type="hidden" name="codigo" id="codigo" value="${depositos.codigoDeposito}"/></td>
													        <td><input type="text" name="address" id="address" value="${depositos.address}" placeholder="Address" /></td>
													        <td><input type="text" name="destination" id="destination" value="${depositos.destination}" placeholder="Destination" /></td>
													        <td><input type="text" name="username" id="username" value="${depositos.username}" placeholder="Username" /></td>
													        <td><input type="text" name="password" id="password" value="${depositos.password}" placeholder="Password" /></td>
													        <td><input type="checkbox" name="estado" value="estado" <c:if test="${depositos.enabled == true}">checked</c:if>><br></td>
															<td><input type="submit" class="button bit" value="Modificar" name="Modificar"></td>
													    </form>
												    </tr>
												</c:forEach>
											</tbody>
										</table>
										<div class="alert <%= request.getAttribute("alert") %>"><% String resp = (String) request.getAttribute("valid");
 																									if (resp != null && (!resp.isEmpty() || !resp.equalsIgnoreCase("null")))
																										out.println(resp); %></div>
									</div>
									<div id="toggler"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Cargar Deposito<span class="caret"></span></button></div>
									<div id="toggled" style="display: none">	
									<form action="/DespachoWeb/AltaDeposito" method="post">
									<h4>Cargar Dep�sito</h4>
									<div class="row uniform">
											
												<div class="12u$">
													<input type="text" name="codigo" id="codigo" value="" placeholder="C�digo" />
												</div>
												<div class="12u$">
													<input type="text" name="address" id="address" value="" placeholder="Address" />
												</div>
												<div class="12u$">
													<input type="text" name="username" id="username" value="" placeholder="Username" />
												</div>
												<div class="12u$">
													<input type="text" name="password" id="password" value="" placeholder="Password" />
												</div>
												<div class="12u$">
													<input type="text" name="destination" id="destination" value="" placeholder="Destination" />
												</div>
													<div class="12u$">
														<div class="select-wrapper">
															<select name="estado" id="estado">
																<option value="0">- Estado -</option>
																<option value="1">Activo</option>
																<option value="2">Inactivo</option>
															</select>
														</div>
													</div>
			
													<ul class="actions">
														<!--  <li><a href="#" class="button big">Aceptar</a></li> -->
														
														<li><input type="submit" class="button bit" value="Aceptar" name="Aceptar"></li>
													</ul>
									</div>
									</form>
									</div>
									<div class="alert <%= request.getAttribute("alert2") %>"><% String resp2 = (String) request.getAttribute("valid2");
 																									if (resp2 != null && (!resp2.isEmpty() || !resp2.equalsIgnoreCase("null")))
																										out.println(resp2); %></div>
									</div>
								
								</section>

						
						</div>
					</div>

				<!-- Sidebar -->
					<div id="sidebar">
						<div class="inner">

							<!-- Menu -->
								<nav id="menu">
									<header class="major">
										<h2>Menu</h2>
									</header>
									<ul>
											<li>
											<span class="opener">Usuario</span>
											<ul>

												<li><a href="/DespachoWeb/jsp/nuevoUsuario.jsp">Nuevo usuario</a></li>
												<li><a href="/DespachoWeb/ListarUsuarios">Listar usuarios</a></li>
												<li><a href="/DespachoWeb/jsp/adminUsuario.jsp">Administrar usuario</a></li>
											</ul>
											</li>
											<li>
											<span class="opener">Articulo/Solicitudes</span>
											<ul>
												<li><a href="/DespachoWeb/jsp/buscararticulo.jsp">Buscar articulo</a></li>
												<li><a href="/DespachoWeb/ListarArticulos">Listar articulo por filtro</a></li>
												<li><a href="/DespachoWeb/jsp/buscarsolicitud.jsp">Buscar solicitudes de articulos</a></li>
												<li><a href="/DespachoWeb/ListarSolicitudes">Listar solicitudes de articulos</a></li>												
											</ul>
										</li>
										<li>
											<span class="opener">Despacho</span>
											<ul>
												<li><a href="/DespachoWeb/jsp/buscarordendespacho.jsp">Buscar ordenes</a></li>
												<li><a href="/DespachoWeb/ListarOrdenDespacho">Listar ordenes</a></li>
											</ul>
										</li>
										<li>
											<span class="opener">M�dulos</span>
											<ul>
												<li><a href="/DespachoWeb/Depositos">Dep�sitos</a></li>
												<li><a href="/DespachoWeb/Portales">Portales Web</a></li>
												<li><a href="/DespachoWeb/Logisticas">Log�stica</a></li>
											</ul>
										</li>
																																				
									</ul>
								</nav>

							<!-- Section -->
								<section>
									<header class="major">
										<h2>P�gina principal</h2>
									</header>
								
									<ul class="actions">
										<li><a href="/DespachoWeb/index.html" class="button">Volver</a></li>
									</ul>
								</section>

						</div>
					</div>

			</div>

		<!-- Scripts -->
			<script src="/DespachoWeb/assets/js/jquery.min.js"></script>
			<script src="/DespachoWeb/assets/js/skel.min.js"></script>
			<script src="/DespachoWeb/assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="/DespachoWeb/assets/js/main.js"></script>

	</body>
</html>