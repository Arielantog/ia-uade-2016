package servlets;

import java.io.IOException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.UsuarioDTO;
import ejbModule.AdminWebLocal;

@WebServlet("/AdminUsuario")
public class AdminUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	AdminWebLocal admin;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminUsuario() {
        super();
        try {
			InitialContext context = new InitialContext();
			admin = (AdminWebLocal) context.lookup("java:global/DespachoG8EAR/DespachoG8//AdminWeb!ejbModule.AdminWebLocal");
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		System.out.println("Llamada a servlet AdminUsuario");
		String usuario = request.getParameter("nombre");
		String status = request.getParameter("estado");
		String alert = "alert-info";
		boolean estado = false;
		if (status.equals("1"))
			estado = true;
		else if (status.equals(2))
			estado = false;
		String valid = "Error al procesar.";
		// Verificar si los campos estan completos
		System.out.println("Info recolectada: Nombre = " + usuario + " - Numero Estado = " + status);
		if (usuario == null || usuario.equalsIgnoreCase("") || status == null || status.equalsIgnoreCase("0"))
			valid = "Faltan datos para cargar el usuario.";
		else if (admin.getUsuario(usuario) != null)
		{
			UsuarioDTO usuarioDTO = admin.getUsuario(usuario);
			usuarioDTO.setEstado(estado);
			admin.modificarUsuario(usuarioDTO);
			valid = "El usuario fue modificado correctamente";
			alert = "alert-success";
		}
		else
		{
			valid = "El usuario no existe";
			alert = "alert-warning";
		
		}
		request.setAttribute("valid", valid);
		request.setAttribute("alert", alert);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/jsp/adminUsuario.jsp");
		dispatcher.forward(request, response);
		
	}

}
