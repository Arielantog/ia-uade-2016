package servlets;

import java.io.IOException;

import javax.naming.InitialContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.OrdenDespachoDTO;
import ejbModule.AdminWebLocal;

/**
 * Servlet implementation class BuscarOrdenDespacho
 */
@WebServlet("/BuscarOrdenDespacho")
public class BuscarOrdenDespacho extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuscarOrdenDespacho() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String alert = "alert-info";
		String valid = "Error";
		String sCodigoOrden = request.getParameter("codigo");
		if (!sCodigoOrden.equals(null) && !sCodigoOrden.equals(""))
		{
			System.out.println("Tengo un codigo de orden de despacho");
			try
			{
				InitialContext context = new InitialContext();
				AdminWebLocal manager = (AdminWebLocal) context.lookup("java:global/DespachoG8EAR/DespachoG8//AdminWeb!ejbModule.AdminWebLocal");
				if (manager.getOrdenDespacho(Integer.parseInt(sCodigoOrden))!=null)
				{
					System.out.println("la orden existe!");
					OrdenDespachoDTO orden = manager.getOrdenDespacho(Integer.parseInt(sCodigoOrden));
					request.setAttribute("orden", orden);
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/buscarordendespacho.jsp");
					dispatcher.forward(request, response);
				}
				else
				{
					System.out.println("la orden no existe");
					valid = "El c�digo de orden de despacho ingresado no existe.";
					alert = "alert-warning";
					request.setAttribute("valid", valid);
					request.setAttribute("alert", alert);
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/buscarordendespacho.jsp");
					dispatcher.forward(request, response);
				}
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println(e.getMessage());
			}
		}
		else
		{
			System.out.println("No tengo un codigo de orden");
			valid = "Ingresar c�digo de orden de despacho.";
			alert = "alert-warning";
			request.setAttribute("valid", valid);
			request.setAttribute("alert", alert);
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/buscarordendespacho.jsp");
			dispatcher.forward(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
