package negocio;

import java.util.ArrayList;
import java.util.List;

import dto.DepositoDTO;

public class Deposito {
	private String codigoDeposito;
	private List<Articulo> articulos;
	private String destination;
	private String username;
	private String password;
	private String address;
	private boolean enabled;
	
	
	public Deposito(String codigoDeposito) {
		this.codigoDeposito = codigoDeposito;
		articulos = new ArrayList<Articulo>();
		this.destination = "";
		this.username = "";
		this.password = "";
		this.address = "";
		this.enabled = true;
	}
	public String getCodigoDeposito() {
		return codigoDeposito;
	}
	public void setCodigoDeposito(String codigoDeposito) {
		this.codigoDeposito = codigoDeposito;
	}
	public List<Articulo> getArticulos() {
		return articulos;
	}
	public void setArticulos(List<Articulo> articulos) {
		this.articulos = articulos;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public DepositoDTO getDTO()
	{
		DepositoDTO deposito = new DepositoDTO();
		deposito.setAddress(this.address);
		deposito.setCodigoDeposito(this.codigoDeposito);
		deposito.setDestination(this.destination);
		deposito.setEnabled(this.enabled);
		deposito.setPassword(this.password);
		deposito.setUsername(this.username);
		return deposito;
	}

}
