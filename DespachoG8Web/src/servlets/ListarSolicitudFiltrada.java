package servlets;

import java.io.IOException;
import java.util.List;

import javax.naming.InitialContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.ArticuloDTO;
import dto.SolicitudArticuloDTO;
import ejbModule.AdminWebLocal;

/**
 * Servlet implementation class ListarSolicitudFiltrada
 */
@WebServlet("/ListarSolicitudFiltrada")
public class ListarSolicitudFiltrada extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListarSolicitudFiltrada() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String alert = "alert-info";
		String valid = "Error";
		String sCodigoDeposito = request.getParameter("filtroDep");
		String sCodigoArticulo = request.getParameter("filtroArt");
		
		if (!sCodigoDeposito.equals("") && !sCodigoArticulo.equals(""))
		{
			System.out.println("tengo codigo de articulo y deposito");
			try
			{
				InitialContext context = new InitialContext();
				AdminWebLocal manager = (AdminWebLocal) context.lookup("java:global/DespachoG8EAR/DespachoG8//AdminWeb!ejbModule.AdminWebLocal");
				if (manager.getSolicitudesDepositoArticulo(sCodigoDeposito, Integer.parseInt(sCodigoArticulo)) != null && manager.getSolicitudesDepositoArticulo(sCodigoDeposito, Integer.parseInt(sCodigoArticulo)).size() != 0)
				{
					System.out.println("tengo solicitudes para ese deposito y articulo");
					List<SolicitudArticuloDTO> solicitudesPorDepositoArticulo = manager.getSolicitudesDepositoArticulo(sCodigoDeposito, Integer.parseInt(sCodigoArticulo));
					request.setAttribute("solicitudes", solicitudesPorDepositoArticulo);
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/filtrosolicitudes.jsp");
					dispatcher.forward(request, response);
				}
				else
				{
					System.out.println("no hay solicitudes para ese deposito y articulo");
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/filtrosolicitudes.jsp");
					dispatcher.forward(request, response);
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println(e.getMessage());
			}
		}
		
		if (!sCodigoDeposito.equals(""))
		{
			System.out.println("Tengo codigo de deposito");
			try
			{
				InitialContext context = new InitialContext();
				AdminWebLocal manager = (AdminWebLocal) context.lookup("java:global/DespachoG8EAR/DespachoG8//AdminWeb!ejbModule.AdminWebLocal");
				if (manager.getSolicitudesDeposito(sCodigoDeposito) != null && manager.getSolicitudesDeposito(sCodigoDeposito).size() != 0)
				{
					System.out.println("Tengo solicitudes para ese deposito");
					List<SolicitudArticuloDTO> solicitudesPorDeposito = manager.getSolicitudesDeposito(sCodigoDeposito);
					request.setAttribute("solicitudes", solicitudesPorDeposito);
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/filtrosolicitudes.jsp");
					dispatcher.forward(request, response);
				}
				else
				{
					System.out.println("no hay solicitudes para ese deposito");
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/filtrosolicitudes.jsp");
					dispatcher.forward(request, response);					
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println(e.getMessage());
			}
		}
		
		if (!sCodigoArticulo.equals(""))
		{	
			System.out.println("Tengo codigo de articulo");
			try
			{
				InitialContext context = new InitialContext();
				AdminWebLocal manager = (AdminWebLocal) context.lookup("java:global/DespachoG8EAR/DespachoG8//AdminWeb!ejbModule.AdminWebLocal");
				if (manager.getSolicitudesArticulo(Integer.parseInt(sCodigoArticulo)) != null && manager.getSolicitudesArticulo(Integer.parseInt(sCodigoArticulo)).size() != 0)
				{
					System.out.println("Tengo solicitudes para ese articulo");
					List<SolicitudArticuloDTO> solicitudesPorArticulo = manager.getSolicitudesArticulo(Integer.parseInt(sCodigoArticulo));
					request.setAttribute("solicitudes", solicitudesPorArticulo);
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/filtrosolicitudes.jsp");
					dispatcher.forward(request, response);
				}
				else
				{
					System.out.println("no hay solicitudes para ese articulo");
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/filtrosolicitudes.jsp");
					dispatcher.forward(request, response);		
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println(e.getMessage());
			}
		}
		
		if(sCodigoDeposito.equals("") && sCodigoArticulo.equals(""))
		{
			System.out.println("no hay nada seleccionado!");
			valid = "Seleccionar un dep�sito y/o art�culo.";
			alert = "alert-warning";
			request.setAttribute("valid", valid);
			request.setAttribute("alert", alert);
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ListarSolicitudes");
			dispatcher.forward(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
