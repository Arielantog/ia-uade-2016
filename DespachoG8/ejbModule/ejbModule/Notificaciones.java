package ejbModule;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import cosasQueVienenDeAfuera.EstadoLogistica;
import cosasQueVienenDeAfuera.EstadoPortal;
import cosasQueVienenDeAfuera.EventoLogistica;
import manager.ManagerPersistencia;
import negocio.Logistica;
import negocio.OrdenDespacho;

@Stateless
@LocalBean
public class Notificaciones{

	@EJB ManagerPersistencia manager;	
	
    public Notificaciones() {

    }

	public boolean notificarEntregaLogistica(Logistica logistica, OrdenDespacho orden) 
	{
		//REST + json
		//Mandamos: id de orden y estado
		//Nos devuelven: verdadero o falso segun resultado
		//Informamos a traves del rest expuesto por logistica
		
		Logger.getAnonymousLogger().info("Entrando a Notificaciones Notificar a Logistica");
		boolean responseLogistica = false;
		try 
		{
			String conn = "http://" +logistica.getAddress() + ":8080/" + logistica.getUrlNotificacionDespacho();
			URL url = new URL(conn);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setDoOutput(true);
			urlConnection.setRequestMethod("POST"); //ESTO ES UN PUT!
			urlConnection.setRequestProperty("Content-Type", "application/json"); 
					
			EstadoLogistica mensaje = new EstadoLogistica();
			mensaje.setIdOrdenDespacho(orden.getPublicCodigoOrdenDespacho());
			mensaje.setEstado("Entregado");
			Gson gson = new Gson();
			String mensajeJson = gson.toJson(mensaje);
					
			IOUtils.write(mensajeJson, urlConnection.getOutputStream());
			if (urlConnection.getResponseCode() != 200)
				throw new RuntimeException("Resultado de conexion: " + urlConnection.getResponseCode());
			
			Logger.getAnonymousLogger().info("Orden notificada a Logistica exitosamente.");
			String response = IOUtils.toString(urlConnection.getInputStream());
			System.out.println("respuesta de Logistica: " + response);
			JsonElement jelement = new JsonParser().parse(response);
		 	JsonObject  json = jelement.getAsJsonObject();
		 	responseLogistica = json.get("resultado").getAsBoolean();
		 	this.notificarEventoLogistica(logistica, "Orden de Despacho notificada a Logistica: " + orden.getPublicCodigoOrdenDespacho() + ".");
			
		} catch (Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
			Logger.getAnonymousLogger().info("Error al notificar a Logistica.");
		}
		
		return responseLogistica;
	}

	public boolean notificarEntregaPortalWeb(OrdenDespacho orden) 
	{
		Logger.getAnonymousLogger().info("Entrando a Notificaciones Notificar a Portal Web");
		URL url;
		boolean responsePortal = false;
		try {
			String conn = "http://" + orden.getPortalWeb().getAddress() + ":8080/" + orden.getPortalWeb().getUrlNotificacionDespacho();
			url = new URL(conn);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setDoOutput(true);
			urlConnection.setRequestMethod("POST");
			urlConnection.setRequestProperty("Content-Type", "application/json");
			
			EstadoPortal mensaje = new EstadoPortal();
			mensaje.setEstado("Entregado");
			mensaje.setnumeroVenta(orden.getIdVenta());
			
			Gson gson = new Gson();
			String mensajeJson = gson.toJson(mensaje);
					
			IOUtils.write(mensajeJson, urlConnection.getOutputStream());
			if (urlConnection.getResponseCode() != 200)
				throw new RuntimeException("Resultado: " + urlConnection.getResponseCode());
			
			Logger.getAnonymousLogger().info("Orden notificada a Portal Web exitosamente.");
			String response = IOUtils.toString(urlConnection.getInputStream());
			System.out.println("respuesta de PortalWeb: " + response);
			JsonElement jelement = new JsonParser().parse(response);
		 	JsonObject  json = jelement.getAsJsonObject();
		 	responsePortal = json.get("resultado").getAsBoolean();
		 	this.notificarEventoLogistica(manager.buscarLogisticaActivo(), "Orden de Despacho notificada a Portal Web: " + orden.getPortalWeb().getCodigoPortalWeb() + ". Codigo de Orden de Despacho: " + orden.getPublicCodigoOrdenDespacho() + ".");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Logger.getAnonymousLogger().info("Error al notificar a Portal Web.");
		}
		return responsePortal;
	}

	public void notificarEventoLogistica(Logistica logistica, String descripcion) 
	{
		//REST y Mensajeria:
		//Mandamos: "{"fecha"":"2002-05-30T09:00:00","tipo":"Portal","modulo":"PortalWebAmazon","descripcion":"Descripcion del evento"}
				
		Logger.getAnonymousLogger().info("Entrando a Notificaciones Notificar Evento a Logistica");
		EventoLogistica mensaje = new EventoLogistica();
		mensaje.setDescripcion(descripcion);
		Date fechaEvento = new Date();
		String sFecha = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(fechaEvento);
		mensaje.setFecha(sFecha);
	
		Gson gson = new Gson();
		String mensajeJson = gson.toJson(mensaje);
		
		//SINCRONICO: REST
		if (logistica.isSync())
		{
			Logger.getAnonymousLogger().info("Notificando Evento por REST...");
			try 
			{
				String conn = "http://" +logistica.getAddress() + ":8080/" + logistica.getUrlEventos();
				URL url = new URL(conn);
				HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
				urlConnection.setDoOutput(true);
				urlConnection.setRequestMethod("POST");
				urlConnection.setRequestProperty("Content-Type", "application/json");
								
				IOUtils.write(mensajeJson, urlConnection.getOutputStream());
				if (urlConnection.getResponseCode() != 200)
					throw new RuntimeException("Resultado: " + urlConnection.getResponseCode());
				
				Logger.getAnonymousLogger().info("Evento notificado a Logistica exitosamente.");
				String response = IOUtils.toString(urlConnection.getInputStream());
				System.out.println("respuesta: " + response);
				//no esperamos respuesta, solo mandamos esto, ya sea por REST o JMS
			} catch (Exception e){
				e.printStackTrace();
				System.out.println(e.getMessage());
				Logger.getAnonymousLogger().info("Error al notificar evento a Logistica.");
			}
		}
		else //ASINCRONICO: MENSAJERIA
		{
			Logger.getAnonymousLogger().info("Notificando Evento por JMS...");
			Connection connection = null;
			try
			{
				final Properties env = new Properties();
				
				env.put("jboss.naming.client.ejb.context", true); 
				env.put("remote.connection.default.connect.options.org.xnio.Options.SASL_POLICY_NOANONYMOUS", "false");
				env.put("remote.connection.default.connect.options.org.xnio.Options.SASL_POLICY_NOPLAINTEXT", "false");
				env.put("remote.connectionprovider.create.options.org.xnio.Options.SSL_ENABLED", "false");				
				env.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
	            env.put(Context.PROVIDER_URL, "http-remoting://" + logistica.getAddress() + ":8080");
	            env.put(Context.SECURITY_PRINCIPAL, logistica.getUsername());
	            env.put(Context.SECURITY_CREDENTIALS, logistica.getPassword());
	            Context namingContext = new InitialContext(env);
							
				ConnectionFactory conFactory = (ConnectionFactory) namingContext.lookup("jms/RemoteConnectionFactory");//porque el cliente esta fuera del proyecto que consume
				connection = conFactory.createConnection(logistica.getUsername(), logistica.getPassword()); //user y pass del AppUser creado con add-user.bat
				Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
				connection.start();
				Destination destination = (Destination) namingContext.lookup(logistica.getDestination());
							
				MessageProducer producer = session.createProducer(destination);
				TextMessage message = session.createTextMessage();
				message.setText(mensajeJson);
				producer.send(message);
				
				System.out.println("Mensaje enviado");
				Logger.getAnonymousLogger().info("Evento notificado a Logistica exitosamente.");
				
				connection.close();
			} catch (Exception e)
			{
				e.printStackTrace();
				System.out.println(e.getMessage());
				Logger.getAnonymousLogger().info("Error al notificar evento a Logistica.");
			}
		}
	}
}
