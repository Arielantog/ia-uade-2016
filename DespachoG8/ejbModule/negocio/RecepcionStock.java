package negocio;

import java.util.Date;

public class RecepcionStock {

	private int codigoRecepcionStock;
	private Articulo articulo;
	private int cantidad;
	private Date fechaRecepcion;
	
	public RecepcionStock(Articulo articulo, int cantidad)
	{
		this.articulo = articulo;
		this.cantidad = cantidad;
		this.fechaRecepcion = new Date();
	}
	
	public int getCodigoRecepcionStock() {
		return codigoRecepcionStock;
	}
	public void setCodigoRecepcionStock(int codigoRecepcionStock) {
		this.codigoRecepcionStock = codigoRecepcionStock;
	}
	public Articulo getArticulo() {
		return articulo;
	}
	public void setArticulo(Articulo articulo) {
		this.articulo = articulo;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public Date getFechaRecepcion() {
		return fechaRecepcion;
	}
	public void setFechaRecepcion(Date fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}
}
