package RSOrdenDespacho;

import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import dto.ArticuloDTO;
import dto.ItemDespachoDTO;
import dto.OrdenDespachoDTO;
import dto.PortalWebDTO;
import dto.RecepcionStockDTO;
import ejbModule.OrdenDespachoLocal;
import ejbModule.RecepcionStockLocal;

@Path("/services")
public class RSServicioOrdenDespacho {	
	
	@POST
	@Path("/recepcionOrdenDespacho")
	@Consumes({"application/json"})
	@Produces({"application/json"})
	public String cargarOrdenDeDespacho(String response)
	{
		
		JsonElement jelement = new JsonParser().parse(response);
	 	JsonObject  json = jelement.getAsJsonObject();
	 	
	 	OrdenDespachoDTO orden = new OrdenDespachoDTO();
	 	PortalWebDTO portal = new PortalWebDTO();
	 	portal.setCodigoPortalWeb(json.get("idPortal").getAsString());
	 	orden.setPortalWeb(portal);
	 	orden.setIdVenta(json.get("idVenta").getAsInt()); //Nos pasan idVenta tambien y lo necesitamos para informarle al Portal
	 	orden.setEstado("PENDIENTE");
	 	orden.setItemsDespacho(new ArrayList<ItemDespachoDTO>());
	 	
	 	JsonArray array = json.getAsJsonArray("detalles");
	 	for (JsonElement element: array)
	 	{
	 		JsonObject object = element.getAsJsonObject();
	 		ItemDespachoDTO item = new ItemDespachoDTO();
	 		item.setCantidad(object.get("cantidad").getAsInt());
	 		ArticuloDTO art = new ArticuloDTO();
	 		art.setCodigoArticulo(object.get("codArticulo").getAsInt());
	 		item.setArticulo(art);
	 		item.setEstado("PENDIENTE");
	 		orden.getItemsDespacho().add(item);
	 	}
	 	
	 	Logger.getAnonymousLogger().info("Codigo Portal Web: " + orden.getPortalWeb().getCodigoPortalWeb() + " - Id de Venta: " + orden.getIdVenta());
	 	for (ItemDespachoDTO item: orden.getItemsDespacho())
	 	{
	 		Logger.getAnonymousLogger().info("Item: Articulo: " + item.getArticulo().getCodigoArticulo() + " - Cantidad: "+ item.getCantidad());
	 	}
	 	
	 	int nroOrden = 0;
	 	InitialContext context;
	 	try {
			context = new InitialContext();
			OrdenDespachoLocal manager = (OrdenDespachoLocal) context.lookup("java:global/DespachoG8EAR/DespachoG8//RecepcionOrdenDespacho!ejbModule.OrdenDespachoLocal");
			
			nroOrden = manager.cargarOrdenDespacho(orden);
			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (nroOrden == 0)
			return "{\"procesado\":\"false\"}";
		else
			return "{\"procesado\":\"true\", \"idOrdenDespacho\":" + nroOrden + "}";
	}
	
	@POST
	@Path("/recepcionStock")
	@Consumes({"application/json"})
	@Produces({"application/json"})
	public String recepcionStock(String response)
	{
		Logger.getAnonymousLogger().info(response);
		JsonElement jelement = new JsonParser().parse(response);
	 	JsonObject  json = jelement.getAsJsonObject();
	 	
	 	RecepcionStockDTO recepcion = new RecepcionStockDTO();
	 	ArticuloDTO art = new ArticuloDTO();
	 	art.setCodigoArticulo(json.get("codArticulo").getAsInt());
	 	recepcion.setArticulo(art);
	 	recepcion.setCantidad(json.get("cantidad").getAsInt());
	 	recepcion.setFechaRecepcion(new Date());
	 	
	 	Logger.getAnonymousLogger().info("Recepcion de Stock: Articulo:" + recepcion.getArticulo().getCodigoArticulo() + " - Cantidad: " + recepcion.getCantidad());
	 	
	 	InitialContext context;
	 	try {
			context = new InitialContext();
			RecepcionStockLocal manager = (RecepcionStockLocal) context.lookup("java:global/DespachoG8EAR/DespachoG8//ManagerRecepcionStock!ejbModule.RecepcionStockLocal");
			
			manager.recibirArticulo(recepcion);
			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 	return "{\"procesado\":\"true\"}";
	}

}
