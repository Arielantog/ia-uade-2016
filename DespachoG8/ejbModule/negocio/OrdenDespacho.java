package negocio;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import dto.ArticuloDTO;
import dto.ItemDespachoDTO;
import dto.OrdenDespachoDTO;
import entities.ArticuloBean;
import entities.ItemDespachoBean;
import entities.OrdenDespachoBean;
import entities.PortalWebBean;

public class OrdenDespacho {
	private int codigoOrdenDespacho;
	private List<ItemDespacho> itemsDespacho;
	private String estado;
	private PortalWeb portalWeb;
	private Date fechaCreacion;
	private int idVenta;
	
	public OrdenDespacho(PortalWeb portalWeb, int idVenta) {
		itemsDespacho = new ArrayList<ItemDespacho>();
		this.estado = "PENDIENTE";
		this.portalWeb = portalWeb;
		this.fechaCreacion = new Date();
		this.idVenta = idVenta;
	}
	
	public OrdenDespacho(PortalWeb portalWeb) {
		itemsDespacho = new ArrayList<ItemDespacho>();
		this.estado = "PENDIENTE";
		this.portalWeb = portalWeb;
		this.fechaCreacion = new Date();
	}
	
	public int getCodigoOrdenDespacho() {
		return codigoOrdenDespacho;
	}

	public void setCodigoOrdenDespacho(int codigoOrdenDespacho) {
		this.codigoOrdenDespacho = codigoOrdenDespacho;
	}

	public List<ItemDespacho> getItemsDespacho() {
		return itemsDespacho;
	}

	public void setItemsDespacho(List<ItemDespacho> itemsDespacho) {
		this.itemsDespacho = itemsDespacho;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public PortalWeb getPortalWeb() {
		return portalWeb;
	}

	public void setPortalWeb(PortalWeb portalWeb) {
		this.portalWeb = portalWeb;
	}
	
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public int getIdVenta() {
		return idVenta;
	}

	public void setIdVenta(int idVenta) {
		this.idVenta = idVenta;
	}

	/****************************FIN GETTERS Y SETTERS************************************/
	
	public int getPublicCodigoOrdenDespacho()
	{
		String publicId = 8 + Integer.toString(this.codigoOrdenDespacho);
		return Integer.parseInt(publicId);
	}
	
	public ItemDespacho agregarItem(Articulo articulo, int cantidad){
		ItemDespacho itemDespacho = new ItemDespacho(articulo, cantidad);
		itemsDespacho.add(itemDespacho);
		return itemDespacho;
	}
	
	public boolean tengoItem (ArticuloDTO articulo, int cantidad){
		boolean ordenLista = false;
		for (ItemDespacho itemDespacho: itemsDespacho){
			if (itemDespacho.getArticulo().getCodigoArticulo() == articulo.getCodigoArticulo()
					&& itemDespacho.getCantidad() == cantidad){
				itemDespacho.setEstado(estado);
				ordenLista = ordenLista();
				if (ordenLista == true)
					actualizarOrden("Listo");
				return true;	
			}
		}
		return false;
	}
	
	private boolean ordenLista() {
		for (ItemDespacho itemDespacho: itemsDespacho)
			if (!itemDespacho.getEstado().equals("Completado"))
				return false;
		return true;
	}

	public void actualizarOrden(String estado){
		setEstado(estado);
	}

	public OrdenDespachoDTO getOrdenDespachoDTO() {
		OrdenDespachoDTO ordenDespacho = new OrdenDespachoDTO();
		ordenDespacho.setCodigoOrdenDespacho(codigoOrdenDespacho);
		List<ItemDespachoDTO> itemsDespachoDTO = new ArrayList<ItemDespachoDTO>();
		for (ItemDespacho itemDespacho: itemsDespacho){
			ItemDespachoDTO itemDespachoDTO = itemDespacho.getItemDespachoDTO();
			itemsDespachoDTO.add(itemDespachoDTO);
		}
		ordenDespacho.setItemsDespacho(itemsDespachoDTO);
		ordenDespacho.setEstado(estado);
		ordenDespacho.setIdVenta(idVenta);
		ordenDespacho.setFechaCreacion(fechaCreacion);
		ordenDespacho.setPortalWeb(portalWeb.getDTO());
		return ordenDespacho;
	}
	
	public ItemDespacho getItemDespacho(Articulo art)
	{
		ItemDespacho item;
		for (ItemDespacho index: this.itemsDespacho)
		{
			if (index.getArticulo().getCodigoArticulo() == art.getCodigoArticulo())
			{
				item = index;
				return item;
			}
		}
		return null;
	}

	public OrdenDespachoBean getOrdenDespachoBean() {
		OrdenDespachoBean ordenDespachoBean = new OrdenDespachoBean();
		ordenDespachoBean.setCodigoOrden(getCodigoOrdenDespacho());
		ordenDespachoBean.setEstado(estado);
		ordenDespachoBean.setFechaCreacion(fechaCreacion);
		ordenDespachoBean.setIdVenta(idVenta);
		
		PortalWebBean portalWebBean = new PortalWebBean();
		portalWebBean.setCodigoPortal(getPortalWeb().getCodigoPortalWeb());
		ordenDespachoBean.setPortalWeb(portalWebBean);
		
		Collection<ItemDespachoBean> itemsDespachoBean = new HashSet<ItemDespachoBean>();
		for (ItemDespacho itemDespacho: getItemsDespacho()){
			ItemDespachoBean itemDespachoBean = new ItemDespachoBean();
			
			itemDespachoBean.setCodigoItemDespacho(itemDespacho.getCodigoItemDespacho());
			
			ArticuloBean articuloBean = new ArticuloBean();
			articuloBean.setCodigoArticulo(itemDespacho.getArticulo().getCodigoArticulo());
			articuloBean.setDescripcion((itemDespacho.getArticulo().getDescripcion()));
			itemDespachoBean.setArticulo(articuloBean);
			
			itemDespachoBean.setCantidad(itemDespacho.getCantidad());
			itemDespachoBean.setEstado(itemDespacho.getEstado());
			itemDespachoBean.setCantidadPendiente(itemDespacho.getCantidadPendiente());
			
			itemsDespachoBean.add(itemDespachoBean);
		}
		ordenDespachoBean.setItemsDespacho(itemsDespachoBean);
		
		return ordenDespachoBean;
	}
	
}
