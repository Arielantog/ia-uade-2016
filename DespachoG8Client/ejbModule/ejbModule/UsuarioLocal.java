package ejbModule;

import javax.ejb.Local;

import dto.UsuarioDTO;


@Local
public interface UsuarioLocal {
	
	public int agregarUsuario(String nombre, boolean estado);
	public UsuarioDTO buscarUsuario(int codigoUsuario);

}
