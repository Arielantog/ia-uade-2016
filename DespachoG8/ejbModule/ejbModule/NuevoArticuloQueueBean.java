package ejbModule;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import manager.ManagerPersistencia;
import negocio.Articulo;
import negocio.Deposito;

/**
 * Message-Driven Bean implementation class for: NuevoArticuloTopicBean
 */
@MessageDriven(
		activationConfig = { @ActivationConfigProperty(
				propertyName = "destination", propertyValue = "java:/jms/queue/DespachoNuevoArticulo"), @ActivationConfigProperty(
				propertyName = "destinationType", propertyValue = "javax.jms.Queue")
		}, 
		mappedName = "java:/jms/queue/DespachoNuevoArticulo")
public class NuevoArticuloQueueBean implements MessageListener {
	
	@EJB private ManagerPersistencia managerPersistencia;
	@EJB private Notificaciones notificar;
    
    public NuevoArticuloQueueBean() {
    
    }
	
	/**
     * @see MessageListener#onMessage(Message)
     */
    public void onMessage(Message message) {
    	
    	try {
    		Logger.getAnonymousLogger().info("Recibiendo nuevo Articulo por JMS");
    		String messageText = null;
   		 	if(message instanceof TextMessage)
   		 	{
   		 		messageText = ((TextMessage)message).getText();
   		 		Logger.getAnonymousLogger().info("Mensaje recibido: " + messageText);
   		 		
   		 		JsonElement jelement = new JsonParser().parse(messageText);
   		 		JsonObject  json = jelement.getAsJsonObject();
   		 		
   		 		String idDeposito = json.get("idDeposito").getAsString();
   		 		int codArticulo = json.get("codArticulo").getAsInt();
   		 		String descripcion = json.get("descripcion").getAsString();
   		 		
   		 		Articulo art = new Articulo(codArticulo, descripcion);
   		 		Deposito depo = managerPersistencia.buscarDeposito(idDeposito);
   		 		Logger.getAnonymousLogger().info("Mensaje recibido: " + art.getCodigoArticulo() + " - " + art.getDescripcion() + " - Deposito: " + depo.getCodigoDeposito());
   		 		try{
   		 			managerPersistencia.nuevoArticulo(art, depo);
   		 			Logger.getAnonymousLogger().info("Articulo agregado exitosamente.");
   		 			notificar.notificarEventoLogistica(managerPersistencia.buscarLogisticaActivo(), "Articulo recibido desde Deposito: " + depo.getCodigoDeposito() + ". Articulo: " + art.getCodigoArticulo() + ".");
   		 		}
   		 		catch(Exception e){
   		 			e.printStackTrace();
   		 			System.out.println(e.getMessage());
   		 			Logger.getAnonymousLogger().info("Error al agregar Articulo.");
   			 	}
   			 
   		 	}
   		 }
   		 catch (Exception e) {
   		 Logger.getAnonymousLogger().log(Level.SEVERE, e.getMessage(), e);
   		 } 
    }
}
