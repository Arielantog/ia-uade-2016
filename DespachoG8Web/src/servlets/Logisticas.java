package servlets;

import java.io.IOException;
import java.util.List;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dto.LogisticaDTO;
import ejbModule.AdminWebLocal;

/**
 * Servlet implementation class Portales
 */
@WebServlet("/Logisticas")
public class Logisticas extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	AdminWebLocal admin;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Logisticas() {
        super();
        try{
        	InitialContext context = new InitialContext();
			admin = (AdminWebLocal) context.lookup("java:global/DespachoG8EAR/DespachoG8//AdminWeb!ejbModule.AdminWebLocal");

        }catch(Exception e){
        	e.printStackTrace();
        }
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		doPost(request, response); //Se llaman entre ellas? D:
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		List<LogisticaDTO> logisticas= admin.getListLogisticas();
		request.setAttribute("logisticas", logisticas);
		request.getRequestDispatcher("/jsp/logisticas.jsp").forward(request, response);
	}

}
