package negocio;

import dto.PortalWebDTO;

public class PortalWeb {
	private String codigoPortalWeb;
	private String address;
	private String urlNotificacionDespacho;
	private boolean enabled;

	public PortalWeb(String codigoPortalWeb) {
		this.codigoPortalWeb = codigoPortalWeb;
	}

	public String getCodigoPortalWeb() {
		return codigoPortalWeb;
	}

	public void setCodigoPortalWeb(String codigoPortalWeb) {
		this.codigoPortalWeb = codigoPortalWeb;
	}

	public PortalWebDTO getDTO() {
		PortalWebDTO portalWeb = new PortalWebDTO();
		portalWeb.setCodigoPortalWeb(codigoPortalWeb);
		portalWeb.setAddress(this.address);
		portalWeb.setEnabled(this.enabled);
		portalWeb.setUrlNotificacionDespacho(this.urlNotificacionDespacho);
		return portalWeb;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUrlNotificacionDespacho() {
		return urlNotificacionDespacho;
	}

	public void setUrlNotificacionDespacho(String urlNotificacionDespacho) {
		this.urlNotificacionDespacho = urlNotificacionDespacho;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	
}
