package negocio;

import dto.UsuarioDTO;
import entities.UsuarioBean;

public class Usuario{
	private Integer codigoUsuario;
	private String nombre;
	private Boolean estado;
	
	public Usuario(String nombre, boolean estado) {
		this.nombre = nombre;
		this.estado = estado;
	}
	public Integer getCodigoUsuario() {
		return codigoUsuario;
	}
	public void setCodigoUsuario(Integer codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public boolean isEstado() {
		return estado;
	}
	public void setEstado(Boolean estado) {
		this.estado = estado;
	}
	
	public UsuarioDTO getDTO(){
		UsuarioDTO usuario = new UsuarioDTO();
		usuario.setCodigoUsuario(codigoUsuario);
		usuario.setEstado(estado);
		usuario.setNombre(nombre);
		return usuario;
	}
	
	public UsuarioBean getBean() {
		UsuarioBean usuario = new UsuarioBean();
		usuario.setCodigoUsuario(codigoUsuario);
		usuario.setEstado(estado);
		usuario.setNombre(nombre);
		return usuario;
	}
	
}