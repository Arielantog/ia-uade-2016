package dto;

import java.io.Serializable;
import java.util.Date;

public class SolicitudArticuloDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private int codigoSolicitudArticulo;
	private DepositoDTO deposito;
	private ArticuloDTO articulo;
	private int cantidad;
	private Date fechaSolicitud;
	
	public int getCodigoSolicitudArticulo() {
		return codigoSolicitudArticulo;
	}
	public void setCodigoSolicitudArticulo(int codigoSolicitudArticulo) {
		this.codigoSolicitudArticulo = codigoSolicitudArticulo;
	}
	public DepositoDTO getDeposito() {
		return deposito;
	}
	public void setDeposito(DepositoDTO deposito) {
		this.deposito = deposito;
	}
	public ArticuloDTO getArticulo() {
		return articulo;
	}
	public void setArticulo(ArticuloDTO articulo) {
		this.articulo = articulo;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}
	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}
}
