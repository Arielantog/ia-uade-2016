package ejbModule;

import java.util.List;

import javax.ejb.Remote;

import dto.DepositoDTO;
import dto.LogisticaDTO;
import dto.PortalWebDTO;
import dto.UsuarioDTO;

@Remote
public interface AdminWebRemote {
	
	// --- USUARIOS --- //
	public int agregarUsuario(UsuarioDTO usuarioDTO);
	public int modificarUsuario(UsuarioDTO usuarioDTO);
	public int eliminarUsuario(UsuarioDTO usuarioDTO);
	public UsuarioDTO getUsuario(int codigoUsuario);
	public UsuarioDTO getUsuario(String nombre);
	public List<UsuarioDTO> getListUsuarios();
	
	// --- PORTALES WEB --- //
	public int agregarPortalWeb(PortalWebDTO portalDTO);
	public int modificarPortalWeb(PortalWebDTO portalDTO);
	public int eliminarPortalWeb(PortalWebDTO portalDTO);
	public PortalWebDTO getPortalWeb(String codigoPortal);
	public List<PortalWebDTO> getListPortales();
	
	// --- DEPOSITOS --- //	
	public int agregarDeposito(DepositoDTO depositoDTO);
	public int modificarDeposito(DepositoDTO depositoDTO);
	public int eliminarDeposito(DepositoDTO depositoDTO);
	public DepositoDTO getDeposito(String codigoDeposito);
	public List<DepositoDTO> getListDepositos();
	
	// --- LOGISTICAS --- //
	public int agregarLogistica(LogisticaDTO logisticaDTO);
	public int modificarLogistica(LogisticaDTO logisticaDTO);
	public int eliminarLogistica(LogisticaDTO logisticaDTO);
	public LogisticaDTO getLogistica(String idLogistica);
	public List<LogisticaDTO> getListLogisticas();

}
