package servlets;

import java.io.IOException;

import javax.naming.InitialContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.ArticuloDTO;
import dto.DepositoDTO;
import ejbModule.AdminWebLocal;

/**
 * Servlet implementation class BuscarArticulo
 */
@WebServlet("/BuscarArticulo")
public class BuscarArticulo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuscarArticulo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String alert = "alert-info";
		String valid = "Error";
		String sCodigoArticulo = request.getParameter("codigo");
		if (!sCodigoArticulo.equals(null) && !sCodigoArticulo.equals(""))
		{
			System.out.println("Tengo un codigo! " + sCodigoArticulo);
			try
			{
				InitialContext context = new InitialContext();
				AdminWebLocal manager = (AdminWebLocal) context.lookup("java:global/DespachoG8EAR/DespachoG8//AdminWeb!ejbModule.AdminWebLocal");
				if (manager.getArticulo(Integer.parseInt(sCodigoArticulo))!=null)
				{
					ArticuloDTO articulo = manager.getArticulo(Integer.parseInt(sCodigoArticulo));
					DepositoDTO deposito = manager.getDepositoArticulo(Integer.parseInt(sCodigoArticulo));
					request.setAttribute("articulo", articulo);
					request.setAttribute("deposito", deposito);
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/buscararticulo.jsp");
					dispatcher.forward(request, response);
					
					
				}
				else
				{
					//El articulo no existe
					System.out.println("El articulo no existe!");
					valid = "El art�culo ingresado no existe.";
					alert = "alert-warning";
					request.setAttribute("valid", valid);
					request.setAttribute("alert", alert);
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/buscararticulo.jsp");
					dispatcher.forward(request, response);
				}
				
			}
			catch (Exception e)
			{
				e.printStackTrace();
				System.out.println(e.getMessage());
			}
		}
		else
		{
			System.out.println("no tengo un codigo");
			valid = "Ingresar c�digo de art�culo.";
			alert = "alert-warning";
			request.setAttribute("valid", valid);
			request.setAttribute("alert", alert);
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/buscararticulo.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		doGet(request, response);
	}

}
