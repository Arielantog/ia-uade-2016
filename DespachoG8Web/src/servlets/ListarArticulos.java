package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.ArticuloDTO;
import dto.DepositoDTO;
import ejbModule.AdminWebLocal;

/**
 * Servlet implementation class ListarArticulos
 */
@WebServlet("/ListarArticulos")
public class ListarArticulos extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListarArticulos() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		try
		{
			InitialContext context = new InitialContext();
			AdminWebLocal manager = (AdminWebLocal) context.lookup("java:global/DespachoG8EAR/DespachoG8//AdminWeb!ejbModule.AdminWebLocal");
			if (manager.getListDepositos() != null && manager.getListDepositos().size() != 0)
			{
				System.out.println("existen depositos!");
				List<DepositoDTO> allDepositos = manager.getListDepositos();
				request.setAttribute("allDepositos", allDepositos);
				
				if (manager.getArticulos() != null && manager.getArticulos().size() != 0)
				{
					System.out.println("existen articulos");
					List<ArticuloDTO> articulos = manager.getArticulos();
					List<DepositoDTO> depositos = new ArrayList<DepositoDTO>();
					for (ArticuloDTO articulo : articulos)
					{
						DepositoDTO deposito = manager.getDepositoArticulo(articulo.getCodigoArticulo());
						depositos.add(deposito);
					}
					request.setAttribute("articulos", articulos);
					request.setAttribute("depositos", depositos);
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/articulofiltro.jsp");
					dispatcher.forward(request, response);
				}
				else
				{
					//Tengo depositos sin articulos. Solo cargo el drop down.
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/articulofiltro.jsp");
					dispatcher.forward(request, response);
				}
			}
			else
			{
				//No tengo ni depositos cargados: no deberia verse nada.
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/articulofiltro.jsp");
				dispatcher.forward(request, response);
			}
			
		}
		catch (Exception e)
		{
			System.out.println("sali por la excepcion???");
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		doGet(request, response);
		
		
	}

}
