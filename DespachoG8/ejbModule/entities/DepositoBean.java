package entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table (name="Depositos")
public class DepositoBean implements Serializable {
	@Id
	private String codigoDeposito;
	@OneToMany
	@JoinColumn(name="codigoDeposito")
	private Collection<ArticuloBean> articulos;
	private String destination;
	private String username;
	private String password;
	private String address;
	private boolean enabled;
	
	public DepositoBean(){
	}
	
	public String getCodigoDeposito() {
		return codigoDeposito;
	}
	public void setCodigoDeposito(String codigoDeposito) {
		this.codigoDeposito = codigoDeposito;
	}
	public Collection<ArticuloBean> getArticulos() {
		return articulos;
	}
	public void setArticulos(Collection<ArticuloBean> articulos) {
		this.articulos = articulos;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
}
