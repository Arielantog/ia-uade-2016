package servlets;

import java.io.IOException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dto.LogisticaDTO;
import ejbModule.AdminWebLocal;


@WebServlet("/ModificarLogistica")
public class ModificarLogistica extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
	AdminWebLocal admin;
	
    public ModificarLogistica() {
        super();
        try {
			InitialContext context = new InitialContext();
			admin = (AdminWebLocal) context.lookup("java:global/DespachoG8EAR/DespachoG8//AdminWeb!ejbModule.AdminWebLocal");
		} catch (NamingException e) {
			e.printStackTrace();
		}
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Llamada a servlet ModificarLogistica");
		
		String codigo = request.getParameter("codigo");
		String address = request.getParameter("address");
		String destination = request.getParameter("destination");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String syncro = request.getParameter("sync");
		String urlEventos = request.getParameter("urlEventos");
		String urlNotificacionDespacho = request.getParameter("urlNotificacionDespacho");
		String status = request.getParameter("estado");
		
		boolean estado = false;
		if (status != null)
			estado = true;

		boolean sync = false;
		if (syncro != null)
			sync = true;
		String valid = "Error al procesar.";
		String alert = "alert-info";
		if (!datosCompletos(codigo, address, destination, username, password, urlEventos, urlNotificacionDespacho))
			valid = "Faltan datos para modificar el dep�sito.";
		else if (admin.getLogistica(codigo) == null)
		{
			valid = "No se puede modificar el c�digo de log�stica";
			alert = "alert-warning";
		}
		else
		{
			LogisticaDTO logistica = new LogisticaDTO();
			logistica.setIdLogistica(codigo);
			logistica.setAddress(address);
			logistica.setDestination(destination);
			logistica.setUsername(username);
			logistica.setPassword(password);
			logistica.setSync(sync);
			logistica.setUrlEventos(urlEventos);
			logistica.setUrlNotificacionDespacho(urlNotificacionDespacho);
			logistica.setEnabled(estado);
			if (admin.modificarLogistica(logistica) != -1)
			{
				valid = "Logistica modificada correctamente.";
				alert = "alert-success";
			}
			else
			{
				valid = "Error al modificar log�stica.";
				alert = "alert-danger";
			}
		}
		request.setAttribute("valid", valid);
		request.setAttribute("alert", alert);
		request.getRequestDispatcher("/Logisticas").forward(request, response);
	}
	
	private boolean datosCompletos(String codigo, String address, String destination, String username, String password, String urlEventos, String urlNotificacionDespacho)
	{
		boolean res = true;
		if (codigo.equalsIgnoreCase(""))
			res = false;
		if (address.equalsIgnoreCase(""))
			res = false;
		if (destination.equalsIgnoreCase(""))
			res = false;
		if (username.equalsIgnoreCase(""))
			res = false;
		if (password.equalsIgnoreCase(""))
			res = false;
		if (urlEventos.equalsIgnoreCase(""))
			res = false;
		if (urlNotificacionDespacho.equalsIgnoreCase(""))
			res = false;
		
		return res;
	}

}
