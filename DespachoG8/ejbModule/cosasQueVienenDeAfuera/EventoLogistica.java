package cosasQueVienenDeAfuera;

public class EventoLogistica {

	private String fecha;
	private String tipo = "Despacho";
	private String modulo = "G08";
	private String descripcion;

	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getTipo() {
		return tipo;
	}
	
	public String getModulo() {
		return modulo;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
