package ejbModule;

import java.util.Hashtable;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.google.gson.Gson;

import cosasQueVienenDeAfuera.MensajeSolicitudArticulo;
import manager.ManagerPersistencia;
import negocio.Deposito;
import negocio.ItemDespacho;
import negocio.OrdenDespacho;
import negocio.SolicitudArticulo;

@Stateless
@LocalBean
public class CrearSolicitudArticulo {

	@EJB private ManagerPersistencia managerPersistencia;
	@EJB private Notificaciones notificar;
	
    public CrearSolicitudArticulo() {
    
    }

	public void generarMensajesSolicitud(OrdenDespacho orden) 
	{
		Logger.getAnonymousLogger().info("Entrando a CrearSolicitudArticulo Generar Mensajes de Solicitud");
		for (ItemDespacho item : orden.getItemsDespacho())
		{
			//Deposito que tiene el articulo:
			Deposito deposito = managerPersistencia.buscarDepositoConArticulo(item.getArticulo().getCodigoArticulo());
			System.out.println("Deposito al que le mando el mensaje: " + deposito.getCodigoDeposito());
			
			SolicitudArticulo solicitud = new SolicitudArticulo(deposito, item.getArticulo(), item.getCantidad());
			
			managerPersistencia.agregarSolicitudArticulo(solicitud);
			
			MensajeSolicitudArticulo mensajeObjeto = new MensajeSolicitudArticulo();
			mensajeObjeto.setCantidad(solicitud.getCantidad());
			mensajeObjeto.setCodArticulo(solicitud.getArticulo().getCodigoArticulo());
			mensajeObjeto.setIdDespacho();
				
			//Armo el mensaje a mandar al deposito: 
			Gson gson = new Gson();
			String mensajeJson = gson.toJson(mensajeObjeto);
			System.out.println("El mensaje a mandar es: " + mensajeJson);
			this.enviarMensaje(deposito, mensajeJson);
		}
		
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	private void enviarMensaje(Deposito deposito, String mensaje)
	{
		try {
			Logger.getAnonymousLogger().info("Entrando a CrearSolicitudArticulo Enviar Mensaje");
			final Hashtable jndiProperties = new Hashtable();
			//jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
			jndiProperties.put("jboss.naming.client.ejb.context", true); 
			jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
			jndiProperties.put(Context.PROVIDER_URL, "http-remoting://" + deposito.getAddress() + ":8080");
			jndiProperties.put(Context.SECURITY_PRINCIPAL, System.getProperty("username", deposito.getUsername()));
			jndiProperties.put(Context.SECURITY_CREDENTIALS, System.getProperty("password", deposito.getPassword()));
			jndiProperties.put("remote.connection.default.connect.options.org.xnio.Options.SASL_POLICY_NOANONYMOUS", "false");
			jndiProperties.put("remote.connection.default.connect.options.org.xnio.Options.SASL_POLICY_NOPLAINTEXT", "false");
			jndiProperties.put("remote.connectionprovider.create.options.org.xnio.Options.SSL_ENABLED", "false");
			//jndiProperties.put("endpoint.name", "client-endpoint");
			System.out.println("Deposito: " + deposito.getCodigoDeposito() + " - JNDI PROPERTIES: " + jndiProperties.get(Context.PROVIDER_URL));
			Context context = new InitialContext(jndiProperties);
			String connectionFactoryString = "jms/RemoteConnectionFactory";
			ConnectionFactory connectionFactory = (ConnectionFactory)context.lookup(connectionFactoryString);
			String destinationString = System.getProperty("destination", deposito.getDestination());
			Destination destination = (Destination) context.lookup(destinationString);
			Connection connection = connectionFactory.createConnection(deposito.getUsername(), deposito.getPassword());
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			connection.start();
			MessageProducer producer = session.createProducer(destination);
			TextMessage message = session.createTextMessage();
			message.setText(mensaje);
			producer.send(message);
			connection.close();
			Logger.getAnonymousLogger().info("Mensaje enviado exitosamente.");
			notificar.notificarEventoLogistica(managerPersistencia.buscarLogisticaActivo(), "Solicitud de Articulo enviada a Deposito: " + deposito.getCodigoDeposito() + ".");
		} catch (NamingException | JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Logger.getAnonymousLogger().info("Error al enviar Solicitud de Articulo.");
		}
	}
}
