package ejbModule;

import javax.ejb.Local;

import dto.RecepcionStockDTO;

@Local
public interface RecepcionStockLocal {
	
	public void recibirArticulo(RecepcionStockDTO recepcionDTO);

}
