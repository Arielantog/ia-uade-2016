package servlets;

import java.io.IOException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dto.PortalWebDTO;
import ejbModule.AdminWebLocal;

/**
 * Servlet implementation class AltaDeposito
 */
@WebServlet("/AltaPortalWeb")
public class AltaPortalWeb extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	AdminWebLocal admin;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AltaPortalWeb() {
        super();
        try {
			InitialContext context = new InitialContext();
			admin = (AdminWebLocal) context.lookup("java:global/DespachoG8EAR/DespachoG8//AdminWeb!ejbModule.AdminWebLocal");
		} catch (NamingException e) {
			e.printStackTrace();
		}
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Llamada a servlet AltaPortalWeb");
		String codigo = request.getParameter("codigo");
		String address = request.getParameter("address");
		String urlNotificacionDespacho = request.getParameter("urlNotificacionDespacho");
		String status = request.getParameter("estado");
		
		boolean estado = false;
		if (status != null)
			estado = true;
		String valid = "Error al procesar.";
		String alert = "alert-info";
		if (!datosCompletos(codigo, address, urlNotificacionDespacho))
			valid = "Faltan datos para crear Portal Web.";
		else if (admin.getPortalWeb(codigo) != null)
		{
			valid = "Ya existe Portal Web con el c�digo ingresado";
			alert = "alert-warning";
		}
		else
		{
			PortalWebDTO portalWeb = new PortalWebDTO();
			portalWeb.setCodigoPortalWeb((codigo));
			portalWeb.setAddress(address);
			portalWeb.setUrlNotificacionDespacho(urlNotificacionDespacho);
			portalWeb.setEnabled(estado);
			if (admin.agregarPortalWeb((portalWeb)) != -1)
			{
				valid = "Portal Web creado correctamente.";
				alert = "alert-success";
			}
			else
			{
				valid = "Error al crear Portal Web.";
				alert = "alert-danger";
			}
		}
		request.setAttribute("valid2", valid);
		request.setAttribute("alert2", alert);
		request.getRequestDispatcher("/Portales").forward(request, response);
	}
	
	private boolean datosCompletos(String codigo, String address, String urlNotificacionDespacho)
	{
		boolean res = true;
		if (codigo.equalsIgnoreCase(""))
			res = false;
		if (address.equalsIgnoreCase(""))
			res = false;
		if (urlNotificacionDespacho.equalsIgnoreCase(""))
			res = false;
		
		return res;
	}

}
