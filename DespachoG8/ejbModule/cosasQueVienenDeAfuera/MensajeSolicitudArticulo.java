package cosasQueVienenDeAfuera;

public class MensajeSolicitudArticulo {

	public static final String DESPACHO = "G08";
	
	@SuppressWarnings("unused")
	private String idDespacho;
	private int codArticulo;
	private int cantidad;
	
	public String getIdDespacho() {
		return DESPACHO;
	}
	public void setIdDespacho() {
		this.idDespacho = DESPACHO;
	}
	public int getCodArticulo() {
		return codArticulo;
	}
	public void setCodArticulo(int codArticulo) {
		this.codArticulo = codArticulo;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
}
