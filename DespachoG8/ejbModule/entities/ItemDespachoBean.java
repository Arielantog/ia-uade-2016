package entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table (name="ItemsOrdenesDespacho")
public class ItemDespachoBean implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int codigoItemDespacho;
	@OneToOne
	@JoinColumn (name="codigoArticulo")
	private ArticuloBean articulo;
	private int cantidad;
	private String estado;
	private int cantidadPendiente;
	
	public int getCantidadPendiente() {
		return cantidadPendiente;
	}
	public void setCantidadPendiente(int cantidadPendiente) {
		this.cantidadPendiente = cantidadPendiente;
	}
	public int getCodigoItemDespacho() {
		return codigoItemDespacho;
	}
	public void setCodigoItemDespacho(int codigoItemDespacho) {
		this.codigoItemDespacho = codigoItemDespacho;
	}
	public ArticuloBean getArticulo() {
		return articulo;
	}
	public void setArticulo(ArticuloBean articulo) {
		this.articulo = articulo;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

}
