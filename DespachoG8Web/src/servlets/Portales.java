package servlets;

import java.io.IOException;
import java.util.List;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.PortalWebDTO;
import ejbModule.AdminWebLocal;


@WebServlet("/Portales")
public class Portales extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	AdminWebLocal admin;
    /**
     * @see HttpServlet#HttpServlet()
     */

    public Portales() {
        super();
        try{
        	InitialContext context = new InitialContext();
			admin = (AdminWebLocal) context.lookup("java:global/DespachoG8EAR/DespachoG8//AdminWeb!ejbModule.AdminWebLocal");

        }catch(Exception e){
        	e.printStackTrace();
        }
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response); 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<PortalWebDTO> portales= admin.getListPortales();
		request.setAttribute("portales", portales);
		request.getRequestDispatcher("/jsp/portales.jsp").forward(request, response);
	}

}
