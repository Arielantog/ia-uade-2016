package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table (name="RecepcionesStock")
public class RecepcionStockBean implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int codigoRecepcionStock;
	@OneToOne
	@JoinColumn(name="codigoArticulo")
	private ArticuloBean articulo;
	private int cantidad;
	private Date fechaRecepcion;
	
	public int getCodigoRecepcionStock() {
		return codigoRecepcionStock;
	}
	public void setCodigoRecepcionStock(int codigoRecepcionStock) {
		this.codigoRecepcionStock = codigoRecepcionStock;
	}
	public ArticuloBean getArticulo() {
		return articulo;
	}
	public void setArticulo(ArticuloBean articulo) {
		this.articulo = articulo;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public Date getFechaRecepcion() {
		return fechaRecepcion;
	}
	public void setFechaRecepcion(Date fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}

	
}
