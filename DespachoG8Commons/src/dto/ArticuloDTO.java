package dto;

import java.io.Serializable;

public class ArticuloDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private int codigoArticulo;
	private String descripcion;
	
	public int getCodigoArticulo() {
		return codigoArticulo;
	}
	public void setCodigoArticulo(int codigoArticulo) {
		this.codigoArticulo = codigoArticulo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
