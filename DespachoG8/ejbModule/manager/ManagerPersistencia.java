package manager;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import dto.OrdenDespachoDTO;
import entities.ArticuloBean;
import entities.DepositoBean;
import entities.ItemDespachoBean;
import entities.LogisticaBean;
import entities.OrdenDespachoBean;
import entities.PortalWebBean;
import entities.RecepcionStockBean;
import entities.SolicitudArticuloBean;
import entities.UsuarioBean;
import negocio.Articulo;
import negocio.Deposito;
import negocio.ItemDespacho;
import negocio.Logistica;
import negocio.OrdenDespacho;
import negocio.PortalWeb;
import negocio.RecepcionStock;
import negocio.SolicitudArticulo;
import negocio.Usuario;

@Stateless
@LocalBean
public class ManagerPersistencia {

    @PersistenceContext(unitName="MyPU")
	private EntityManager manager;
    
	public ManagerPersistencia() {

    }
    
	/*********************************** DEPOSITOS ***********************************/
	
	public void agregarDeposito(Deposito deposito){
		Logger.getAnonymousLogger().info("Entrando a Manager Persistencia Agregar Deposito");
		DepositoBean depositoBean = new DepositoBean();
		depositoBean.setCodigoDeposito(deposito.getCodigoDeposito());
		depositoBean.setAddress(deposito.getAddress());
		depositoBean.setDestination(deposito.getDestination());
		depositoBean.setEnabled(deposito.isEnabled());
		depositoBean.setPassword(deposito.getPassword());
		depositoBean.setUsername(deposito.getUsername());
		try{
			manager.persist(depositoBean);
			Logger.getAnonymousLogger().info("Deposito persistido correctamente.");
		}
		catch(Exception e){
			e.printStackTrace();
    		System.out.println(e.getMessage());
    		Logger.getAnonymousLogger().info("Error al persistir Deposito.");
		}
	}
	
	public Deposito buscarDepositoConArticulo(int codigoArticulo)
	{
		Logger.getAnonymousLogger().info("Entrando a Manager Persistencia Buscar Deposito con Articulo");
		String query = "select d from DepositoBean d, in (d.articulos) as a where a.codigoArticulo = :codigoArticulo";
		DepositoBean depositoBean = (DepositoBean) manager.createQuery(query).setParameter("codigoArticulo", codigoArticulo).getSingleResult();
		Deposito deposito = new Deposito(depositoBean.getCodigoDeposito());
		deposito.setAddress(depositoBean.getAddress());
		deposito.setDestination(depositoBean.getDestination());
		deposito.setEnabled(depositoBean.isEnabled());
		deposito.setPassword(depositoBean.getPassword());
		deposito.setUsername(depositoBean.getUsername());
		Logger.getAnonymousLogger().info("Devolviendo Deposito...");
		return deposito;
	}
	
	public Deposito buscarDeposito(String codigoDeposito)
	{
		Logger.getAnonymousLogger().info("Entrando a Manager Persistencia Buscar Deposito por Codigo");
		DepositoBean depositoBean = null;
		try{
		depositoBean = manager.find(DepositoBean.class, codigoDeposito);
		}
		catch (NoResultException e)
		{
			System.out.println(e.getStackTrace());
			return null;
		}
		if (depositoBean != null)
		{
			Logger.getAnonymousLogger().info("Deposito Encontrado: " + depositoBean.getCodigoDeposito());
			Deposito deposito = new Deposito(depositoBean.getCodigoDeposito());
			deposito.setAddress(depositoBean.getAddress());
			deposito.setDestination(depositoBean.getDestination());
			deposito.setEnabled(depositoBean.isEnabled());
			deposito.setPassword(depositoBean.getPassword());
			deposito.setUsername(depositoBean.getUsername());
			Logger.getAnonymousLogger().info("Devolviendo Deposito...");
		return deposito;
		}
		else
		{
			Logger.getAnonymousLogger().info("Deposito no encontrado.");
			return null;
		}
	}
	
	public List<Deposito> getListDepositos()
	{
		Logger.getAnonymousLogger().info("Entrando a Manager Persistencia Listar Depositos");
		try{
			String query = "SELECT d "
					+ "FROM DepositoBean d ";
		
			@SuppressWarnings("unchecked")
			List<DepositoBean> listaBeans = manager.createQuery(query).getResultList();
			List<Deposito> lista = new ArrayList<Deposito>();
			if(listaBeans != null)
			{
				for (DepositoBean bean: listaBeans)
				{
					Deposito depo = new Deposito(bean.getCodigoDeposito());
					depo.setAddress(bean.getAddress());
					depo.setDestination(bean.getDestination());
					depo.setEnabled(bean.isEnabled());
					depo.setPassword(bean.getPassword());
					depo.setUsername(bean.getUsername());
					lista.add(depo);
				}
			}
			
			Logger.getAnonymousLogger().info("Devolviendo lista de Depositos.");
			return lista;
		}catch(Exception e){
			System.out.println(e);
			Logger.getAnonymousLogger().info("Error al obtener lista de Depositos.");
			return null;
		}
	}
	
	public void actualizarDeposito(Deposito deposito)
	{
		Logger.getAnonymousLogger().info("Entrando a Manager Persistencia Actualizar Deposito");
		DepositoBean depositoBean = manager.find(DepositoBean.class, deposito.getCodigoDeposito());
		depositoBean.setAddress(deposito.getAddress());
		depositoBean.setDestination(deposito.getDestination());
		depositoBean.setEnabled(deposito.isEnabled());
		depositoBean.setPassword(deposito.getPassword());
		depositoBean.setUsername(deposito.getUsername());
		manager.merge(depositoBean);
		Logger.getAnonymousLogger().info("Deposito actualizado exitosamente.");
	}
	
	public void eliminarDeposito(Deposito deposito)
	{
		Logger.getAnonymousLogger().info("Entrando a Manager Persistencia Eliminar Deposito");
		DepositoBean depo = manager.find(DepositoBean.class, deposito.getCodigoDeposito());
		if (depo != null)
			manager.remove(depo);
		Logger.getAnonymousLogger().info("Deposito eliminado.");
	}
	
	@SuppressWarnings("unchecked")
	public List<Articulo> buscarArticulosPorDeposito(String codigoDeposito)
	{
		Logger.getAnonymousLogger().info("Entrando a Manager Persistencia Buscar Articulos por Deposito");
		String query = "select d.articulos from DepositoBean d where d.codigoDeposito = :codigoDeposito";
		List<ArticuloBean> articulosBean = manager.createQuery(query).setParameter("codigoDeposito", codigoDeposito).getResultList();
		List<Articulo> articulos = new ArrayList<Articulo>();
		if (articulosBean != null)
		{
			for(ArticuloBean articuloBean : articulosBean)
			{
				Articulo articulo = articuloBean.getNegocio();
				articulos.add(articulo);
			}
			Logger.getAnonymousLogger().info("Devolviendo Articulos.");
			return articulos;
		}
		else
		{
			Logger.getAnonymousLogger().info("Error al buscar Ariculos por Deposito.");
			return null;
		}
	}
	
		
	/*********************************** ARTICULOS ***********************************/

	public void nuevoArticulo(Articulo articulo, Deposito deposito) throws Exception{
		
		DepositoBean depositoBean = manager.find(DepositoBean.class, deposito.getCodigoDeposito());
		if (depositoBean == null)
		{
			throw new Exception("No se encuentra el deposito");
		}
		ArticuloBean articuloBean = new ArticuloBean();
		articuloBean.setCodigoArticulo(articulo.getCodigoArticulo());
		articuloBean.setDescripcion(articulo.getDescripcion());
		depositoBean.getArticulos().add(articuloBean);
		try{
			manager.persist(articuloBean);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
	
	public Articulo buscarArticulo(int codigoArticulo) {
		ArticuloBean articuloBean = manager.find(ArticuloBean.class, codigoArticulo);
		if (articuloBean != null)
		{	
			Articulo articulo = new Articulo(articuloBean.getCodigoArticulo(), articuloBean.getDescripcion());
			return articulo;
		}
		else
			return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<Articulo> buscarArticulos() {
		String query = "select a from ArticuloBean a";
		List<Articulo> articulos = new ArrayList<Articulo>();
		List<ArticuloBean> articulosBean = manager.createQuery(query).getResultList();
		if (articulosBean != null)
		{
			for (ArticuloBean articuloBean : articulosBean)
			{
				Articulo articulo = new Articulo(articuloBean.getCodigoArticulo(),articuloBean.getDescripcion());
				articulos.add(articulo);
			}
			return articulos;
		}
		else 
			return null;
	}
	
	/*********************************** USUARIOS ***********************************/
	
	public int agregarUsuario(Usuario usuario){
		UsuarioBean usuarioBean = new UsuarioBean();
		usuarioBean.setNombre(usuario.getNombre());
		usuarioBean.setEstado(usuario.isEstado());
		try{
			manager.persist(usuarioBean);
			return usuarioBean.getCodigoUsuario();
		}
		catch(Exception e){
			e.printStackTrace();
    		System.out.println(e.getMessage());
		}
		return -1;
	}

	public Usuario buscarUsuario(int codigoUsuario) {
		UsuarioBean usuarioBean = manager.find(UsuarioBean.class, codigoUsuario);
		if (usuarioBean != null)
		{
			Usuario usuario = new Usuario(usuarioBean.getNombre(), usuarioBean.isEstado());
			usuario.setCodigoUsuario(usuarioBean.getCodigoUsuario());
			return usuario;
		}
		else
			return null;
	}
	
	public Usuario buscarUsuario(String nombre) {
		String query = "SELECT usuario "
				+ "FROM UsuarioBean usuario "
				+ "WHERE usuario.nombre = ? "
						;
		UsuarioBean usuarioBean;
		try{
		usuarioBean = (UsuarioBean) manager.createQuery(query).setParameter(1, nombre).setMaxResults(1).getSingleResult();
		}
		catch(NoResultException e)
		{
			e.getStackTrace();
			return null;
		}
		if (usuarioBean != null)
		{
			Usuario usuario = new Usuario(usuarioBean.getNombre(), usuarioBean.isEstado());
			usuario.setCodigoUsuario(usuarioBean.getCodigoUsuario());
			return usuario;
		}
		else
			return null;
	}
	
	public List<Usuario> getListUsuarios()
	{
		try{
			String query = "SELECT u "
					+ "FROM UsuarioBean u ";
		
			@SuppressWarnings("unchecked")
			List<UsuarioBean> listaBeans = manager.createQuery(query).getResultList();
			List<Usuario> lista = new ArrayList<Usuario>();
			if(listaBeans != null)
			{
				for (UsuarioBean bean: listaBeans)
				{
					Usuario user = new Usuario(bean.getNombre(), bean.isEstado());
					user.setCodigoUsuario(bean.getCodigoUsuario());
					lista.add(user);
				}
			}
			
			return lista;
		}catch(Exception e){
			System.out.println(e);
			return null;
		}
	}
	
	public void actualizarUsuario(Usuario usuario) {
		UsuarioBean usuarioBean = manager.find(UsuarioBean.class, usuario.getCodigoUsuario());
		if (usuarioBean != null)
		{
			usuarioBean.setEstado(usuario.isEstado());
			usuarioBean.setNombre(usuario.getNombre());
			manager.merge(usuarioBean);
		}
	}
	
	public void eliminarUsuario(Usuario usuario)
	{
		UsuarioBean usuariobean = manager.find(UsuarioBean.class, usuario.getCodigoUsuario());
		if (usuariobean != null)
			manager.remove(usuariobean);
	}

	/*********************************** ORDENES DE DESPACHO ***********************************/
	
	public int nuevarOrdenDespacho(OrdenDespacho ordenDespacho) 
	{
		OrdenDespachoBean ordenDespachoBean = new OrdenDespachoBean();
		ordenDespachoBean.setEstado("PENDIENTE");
		PortalWebBean portalBean = manager.find(PortalWebBean.class, ordenDespacho.getPortalWeb().getCodigoPortalWeb());
		ordenDespachoBean.setPortalWeb(portalBean);
		ordenDespachoBean.setIdVenta(ordenDespacho.getIdVenta());
		//Fecha Creacion: 
		ordenDespachoBean.setFechaCreacion(ordenDespacho.getFechaCreacion());
		ordenDespachoBean.setItemsDespacho(new ArrayList<ItemDespachoBean>());
		manager.persist(ordenDespachoBean); 
		for (ItemDespacho item: ordenDespacho.getItemsDespacho())
		{
			ItemDespachoBean itemBean = new ItemDespachoBean();
			itemBean.setArticulo(manager.find(ArticuloBean.class, item.getArticulo().getCodigoArticulo()));
			itemBean.setCantidad(item.getCantidad());
			itemBean.setEstado("PENDIENTE");
			itemBean.setCantidadPendiente(itemBean.getCantidad());
			ordenDespachoBean.getItemsDespacho().add(itemBean);
			manager.persist(itemBean);
		}
		
		return ordenDespachoBean.getCodigoOrden();
	}

	public OrdenDespacho buscarOrdenDespacho(int codigoOrdenDespacho) 
	{
		OrdenDespachoBean ordenDespachoBean = manager.find(OrdenDespachoBean.class, codigoOrdenDespacho);
		if (ordenDespachoBean != null)
		{	
			PortalWeb portalWeb = new PortalWeb(ordenDespachoBean.getPortalWeb().getCodigoPortal());
			OrdenDespacho ordenDespacho = new OrdenDespacho(portalWeb);
			ordenDespacho.setCodigoOrdenDespacho(ordenDespachoBean.getCodigoOrden());
			ordenDespacho.setEstado(ordenDespachoBean.getEstado());
			ordenDespacho.setFechaCreacion(ordenDespachoBean.getFechaCreacion());
			ordenDespacho.setIdVenta(ordenDespachoBean.getIdVenta());
		
			List<ItemDespacho> itemsDespacho = new ArrayList<ItemDespacho>();
			for (ItemDespachoBean itemDespachoBean : ordenDespachoBean.getItemsDespacho())
			{
				Articulo articulo = new Articulo(itemDespachoBean.getArticulo().getCodigoArticulo(), itemDespachoBean.getArticulo().getDescripcion());
				int cantidad = itemDespachoBean.getCantidad();
				ItemDespacho itemDespacho = new ItemDespacho(articulo, cantidad);
			
				itemDespacho.setCodigoItemDespacho(itemDespachoBean.getCodigoItemDespacho());
				itemDespacho.setEstado(itemDespachoBean.getEstado());
				itemDespacho.setCantidadPendiente(itemDespachoBean.getCantidadPendiente());
				itemsDespacho.add(itemDespacho);
			}
			ordenDespacho.setItemsDespacho(itemsDespacho);
			return ordenDespacho;
		}
		else
			return null;
	}
	
	public List<OrdenDespachoDTO> getListOrdenes() {
		try{
			String query = "SELECT orden "
					+ "FROM OrdenDespachoBean orden ";
		
			@SuppressWarnings("unchecked")
			List<OrdenDespachoBean> listaBeans = manager.createQuery(query).getResultList();
			List<OrdenDespachoDTO> lista = new ArrayList<OrdenDespachoDTO>();
			if(listaBeans != null)
			{
				for (OrdenDespachoBean bean: listaBeans)
				{
					OrdenDespachoDTO ordenDespacho = new OrdenDespachoDTO();
					ordenDespacho.setCodigoOrdenDespacho(bean.getCodigoOrden());
					ordenDespacho.setEstado(bean.getEstado());
					ordenDespacho.setFechaCreacion(bean.getFechaCreacion());
					ordenDespacho.setIdVenta(bean.getIdVenta());
					ordenDespacho.setPortalWeb(bean.getPortalWeb().getNegocio().getDTO());
					//ordenDespacho.setItemsDespacho(); NO HICE EL FOR, NO LO NECESITABA
					lista.add(ordenDespacho);
				}
			}
			
			return lista;
		}catch(Exception e){
			System.out.println(e);
			return null;
		}
	}
	
	/*********************************** PORTALES WEB ***********************************/
	
	public void agregarPortalWeb(PortalWeb portalWeb) 
	{
		PortalWebBean portalBean = new PortalWebBean();
		portalBean.setCodigoPortal(portalWeb.getCodigoPortalWeb());
		portalBean.setAddress(portalWeb.getAddress());
		portalBean.setEnabled(portalWeb.isEnabled());
		portalBean.setUrlNotificacionDespacho(portalWeb.getUrlNotificacionDespacho());
		manager.persist(portalBean);		
	}
	
	public PortalWeb buscarPortalWeb (String codigoPortalWeb){
		PortalWebBean portalWebBean = manager.find(PortalWebBean.class, codigoPortalWeb);
		if (portalWebBean != null)
		{
			PortalWeb portalWeb = new PortalWeb(portalWebBean.getCodigoPortal());
			portalWeb.setAddress(portalWebBean.getAddress());
			portalWeb.setEnabled(portalWebBean.isEnabled());
			portalWeb.setUrlNotificacionDespacho(portalWebBean.getUrlNotificacionDespacho());
			return portalWeb;
		}
		else
			return null;
	}
	
	public List<PortalWeb> getListPortalesWeb()
	{
		try{
			String query = "SELECT p "
					+ "FROM PortalWebBean p ";
		
			@SuppressWarnings("unchecked")
			List<PortalWebBean> listaBeans = manager.createQuery(query).getResultList();
			List<PortalWeb> lista = new ArrayList<PortalWeb>();
			if(listaBeans != null)
			{
				for (PortalWebBean bean: listaBeans)
				{
					PortalWeb portal = new PortalWeb(bean.getCodigoPortal());
					portal.setAddress(bean.getAddress());
					portal.setEnabled(bean.isEnabled());
					portal.setUrlNotificacionDespacho(bean.getUrlNotificacionDespacho());
					lista.add(portal);
				}
			}
			
			return lista;
		}catch(Exception e){
			System.out.println(e);
			return null;
		}
	}
	
	public void actualizarPortalWeb(PortalWeb portal)
	{
		PortalWebBean portalBean = manager.find(PortalWebBean.class, portal.getCodigoPortalWeb());
		if (portalBean != null)
		{
			portalBean.setAddress(portal.getAddress());
			portalBean.setEnabled(portal.isEnabled());
			portalBean.setUrlNotificacionDespacho(portal.getUrlNotificacionDespacho());
			manager.merge(portalBean);
		}
	}
	
	public void eliminarPortalWeb(PortalWeb portal)
	{
		PortalWebBean portalBean = manager.find(PortalWebBean.class, portal.getCodigoPortalWeb());
		if (portalBean != null)
			manager.remove(portalBean);
	}

	/*********************************** SOLICITUD ARTICULO ***********************************/
	
	public void agregarSolicitudArticulo(SolicitudArticulo solicitud) {
		ArticuloBean articuloBean = manager.find(ArticuloBean.class, solicitud.getArticulo().getCodigoArticulo());
		DepositoBean depositoBean = manager.find(DepositoBean.class, solicitud.getDeposito().getCodigoDeposito());
		SolicitudArticuloBean solicitudArticuloBean = new SolicitudArticuloBean();
		solicitudArticuloBean.setDeposito(depositoBean);
		solicitudArticuloBean.setArticulo(articuloBean);
		solicitudArticuloBean.setCantidad(solicitud.getCantidad());
		solicitudArticuloBean.setFechaSolicitud(solicitud.getFechaSolicitud());
		manager.persist(solicitudArticuloBean);
		
		
	}
	
	@SuppressWarnings("unchecked")
	public List<SolicitudArticulo> buscarSolicitudes() 
	{
		String query = "select s from SolicitudArticuloBean s";
		List<SolicitudArticulo> solicitudes = new ArrayList<SolicitudArticulo>();
		List<SolicitudArticuloBean> solicitudesBean = manager.createQuery(query).getResultList();
		if (solicitudesBean != null)
		{
			for (SolicitudArticuloBean solicitudBean : solicitudesBean)
			{
				DepositoBean depositoBean = manager.find(DepositoBean.class, solicitudBean.getDeposito().getCodigoDeposito());
				Deposito deposito = new Deposito(depositoBean.getCodigoDeposito());
				ArticuloBean articuloBean = manager.find(ArticuloBean.class, solicitudBean.getArticulo().getCodigoArticulo());
				Articulo articulo = articuloBean.getNegocio();
				SolicitudArticulo solicitud = new SolicitudArticulo(deposito, articulo, solicitudBean.getCantidad());
				solicitud.setFechaSolicitud(solicitudBean.getFechaSolicitud());
				solicitud.setCodigoSolicitudArticulo(solicitudBean.getCodigoSolicitudArticulo());
				solicitudes.add(solicitud);
			}
			return solicitudes;
		}
		else 
			return null;
	}
	
	public SolicitudArticulo buscarSolicitud(int nroSolicitud) {
		SolicitudArticuloBean solicitudBean = manager.find(SolicitudArticuloBean.class, nroSolicitud);
		if(solicitudBean != null)
		{
			DepositoBean depositoBean = manager.find(DepositoBean.class, solicitudBean.getDeposito().getCodigoDeposito());
			Deposito deposito = new Deposito(depositoBean.getCodigoDeposito());
			ArticuloBean articuloBean = manager.find(ArticuloBean.class, solicitudBean.getArticulo().getCodigoArticulo());
			Articulo articulo = articuloBean.getNegocio();
			SolicitudArticulo solicitud = new SolicitudArticulo(deposito, articulo, solicitudBean.getCantidad());
			solicitud.setFechaSolicitud(solicitudBean.getFechaSolicitud());
			solicitud.setCodigoSolicitudArticulo(solicitudBean.getCodigoSolicitudArticulo());
			return solicitud;
		}
		else
			return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<SolicitudArticulo> buscarSolicitudesPorDepositoArticulo(String codigoDeposito, int codigoArticulo)
	{
		String query = "select s from SolicitudArticuloBean s where s.deposito.codigoDeposito = :deposito and s.articulo.codigoArticulo = :articulo";
		List<SolicitudArticulo> solicitudes = new ArrayList<SolicitudArticulo>();
		List<SolicitudArticuloBean> solicitudesBean = manager.createQuery(query).setParameter("deposito", codigoDeposito).setParameter("articulo", codigoArticulo).getResultList();
		if (solicitudesBean != null)
		{
			for (SolicitudArticuloBean solicitudBean : solicitudesBean)
			{
				DepositoBean depositoBean = manager.find(DepositoBean.class, solicitudBean.getDeposito().getCodigoDeposito());
				Deposito deposito = new Deposito(depositoBean.getCodigoDeposito());
				ArticuloBean articuloBean = manager.find(ArticuloBean.class, solicitudBean.getArticulo().getCodigoArticulo());
				Articulo articulo = articuloBean.getNegocio();
				SolicitudArticulo solicitud = new SolicitudArticulo(deposito, articulo, solicitudBean.getCantidad());
				solicitud.setFechaSolicitud(solicitudBean.getFechaSolicitud());
				solicitud.setCodigoSolicitudArticulo(solicitudBean.getCodigoSolicitudArticulo());
				solicitudes.add(solicitud);
			}
			return solicitudes;
		}
		else 
			return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<SolicitudArticulo> buscarSolicitudesPorDeposito(String codigoDeposito)
	{
		String query = "select s from SolicitudArticuloBean s where s.deposito.codigoDeposito = :deposito";
		List<SolicitudArticulo> solicitudes = new ArrayList<SolicitudArticulo>();
		List<SolicitudArticuloBean> solicitudesBean = manager.createQuery(query).setParameter("deposito", codigoDeposito).getResultList();
		if (solicitudesBean != null)
		{
			for (SolicitudArticuloBean solicitudBean : solicitudesBean)
			{
				DepositoBean depositoBean = manager.find(DepositoBean.class, solicitudBean.getDeposito().getCodigoDeposito());
				Deposito deposito = new Deposito(depositoBean.getCodigoDeposito());
				ArticuloBean articuloBean = manager.find(ArticuloBean.class, solicitudBean.getArticulo().getCodigoArticulo());
				Articulo articulo = articuloBean.getNegocio();
				SolicitudArticulo solicitud = new SolicitudArticulo(deposito, articulo, solicitudBean.getCantidad());
				solicitud.setFechaSolicitud(solicitudBean.getFechaSolicitud());
				solicitud.setCodigoSolicitudArticulo(solicitudBean.getCodigoSolicitudArticulo());
				solicitudes.add(solicitud);
			}
			return solicitudes;
		}
		else 
			return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<SolicitudArticulo> buscarSolicitudesPorArticulo(int codigoArticulo)
	{
		String query = "select s from SolicitudArticuloBean s where s.articulo.codigoArticulo = :articulo";
		List<SolicitudArticulo> solicitudes = new ArrayList<SolicitudArticulo>();
		List<SolicitudArticuloBean> solicitudesBean = manager.createQuery(query).setParameter("articulo", codigoArticulo).getResultList();
		if (solicitudesBean != null)
		{
			for (SolicitudArticuloBean solicitudBean : solicitudesBean)
			{
				DepositoBean depositoBean = manager.find(DepositoBean.class, solicitudBean.getDeposito().getCodigoDeposito());
				Deposito deposito = new Deposito(depositoBean.getCodigoDeposito());
				ArticuloBean articuloBean = manager.find(ArticuloBean.class, solicitudBean.getArticulo().getCodigoArticulo());
				Articulo articulo = articuloBean.getNegocio();
				SolicitudArticulo solicitud = new SolicitudArticulo(deposito, articulo, solicitudBean.getCantidad());
				solicitud.setFechaSolicitud(solicitudBean.getFechaSolicitud());
				solicitud.setCodigoSolicitudArticulo(solicitudBean.getCodigoSolicitudArticulo());
				solicitudes.add(solicitud);
			}
			return solicitudes;
		}
		else 
			return null;
	}

	/*********************************** RECEPCION DE STOCK DE ARTICULO ***********************************/
	
	public int agregarRecepcionStock(RecepcionStock recepcion)
	{
		ArticuloBean art = manager.find(ArticuloBean.class, recepcion.getArticulo().getCodigoArticulo());
		RecepcionStockBean recepcionBean = new RecepcionStockBean();
		recepcionBean.setArticulo(art);
		recepcionBean.setCantidad(recepcion.getCantidad());
		recepcionBean.setFechaRecepcion(recepcion.getFechaRecepcion());
		manager.persist(recepcionBean);
		
		return recepcionBean.getCodigoRecepcionStock();
		
	}

	public OrdenDespacho buscarOrdenActualizar(int codigoArticulo) {
		try{
			String query = "SELECT orden "
					+ "FROM OrdenDespachoBean orden "
					+ "INNER JOIN orden.itemsDespacho item "
					+ "INNER JOIN item.articulo articulo "
					+ "WHERE orden.estado = ? "
					+ "AND item.estado = ? "
					+ "AND articulo.codigoArticulo = ? "
							;
		
			OrdenDespachoBean ordenDespachoBean = (OrdenDespachoBean) manager.createQuery(query).setParameter(1, "PENDIENTE").setParameter(2, "PENDIENTE").setParameter(3, codigoArticulo).setMaxResults(1).getSingleResult();
	
			return ordenDespachoBean.getNegocio();
		}catch(Exception e){
			System.out.println(e);
		}
		
		return null;
		
	}

	public void actualizarItemDespacho(ItemDespacho itemDespacho) {
		ItemDespachoBean itemDespachoBean = itemDespacho.getBean();
		manager.merge(itemDespachoBean);
	}

	public void actualizarOrdenDespacho(OrdenDespacho ordenDespacho) {
		try{
			OrdenDespachoBean ordenDespachoBean = ordenDespacho.getOrdenDespachoBean();
			manager.merge(ordenDespachoBean);
		}catch(Exception e){
			System.out.println(e);
		}
		
	}

	@SuppressWarnings("unchecked")
	public List<OrdenDespacho> buscarOrdenesLogistica() {
		try{
			String query = "SELECT orden "
					+ "FROM OrdenDespachoBean orden "
					+ "WHERE orden.estado = ? "
							;
		
			List<OrdenDespachoBean> ordenesDespachoBean = new ArrayList<OrdenDespachoBean>();
			ordenesDespachoBean = manager.createQuery(query).setParameter(1, "ENTREGADA PORTAL").getResultList();
			List<OrdenDespacho> ordenesDespacho = new ArrayList<OrdenDespacho>();
			for (OrdenDespachoBean ordenDespachoBean: ordenesDespachoBean)
				ordenesDespacho.add(ordenDespachoBean.getNegocio());
			return ordenesDespacho;
		}catch(Exception e){
			System.out.println(e);
		}
		List<OrdenDespacho> ordenesDespacho = new ArrayList<OrdenDespacho>();
		return ordenesDespacho;
	}

	@SuppressWarnings("unchecked")
	public List<OrdenDespacho> buscarOrdenesPortal() {
		try{
			String query = "SELECT orden "
					+ "FROM OrdenDespachoBean orden "
					+ "WHERE orden.estado = ? "
							;
		
			List<OrdenDespachoBean> ordenesDespachoBean = new ArrayList<OrdenDespachoBean>();
			ordenesDespachoBean = manager.createQuery(query).setParameter(1, "LISTO").getResultList();
			List<OrdenDespacho> ordenesDespacho = new ArrayList<OrdenDespacho>();
			for (OrdenDespachoBean ordenDespachoBean: ordenesDespachoBean)
				ordenesDespacho.add(ordenDespachoBean.getNegocio());
			return ordenesDespacho;
		}catch(Exception e){
			System.out.println(e);
		}
		List<OrdenDespacho> ordenesDespacho = new ArrayList<OrdenDespacho>();
		return ordenesDespacho;
	}

	/*********************************** LOGISTICA ***********************************/
	
	public void agregarLogistica(Logistica logistica)
	{
		LogisticaBean logisticaBean = new LogisticaBean();
		logisticaBean.setAddress(logistica.getAddress());
		logisticaBean.setEnabled(logistica.isEnabled());
		logisticaBean.setIdLogistica(logistica.getIdLogistica());
		logisticaBean.setUrlEventos(logistica.getUrlEventos());
		logisticaBean.setUrlNotificacionDespacho(logistica.getUrlNotificacionDespacho());
		logisticaBean.setSync(logistica.isSync());
		logisticaBean.setDestination(logistica.getDestination());
		logisticaBean.setUsername(logistica.getUsername());
		logisticaBean.setPassword(logistica.getPassword());
		manager.persist(logisticaBean);
	}
	
	public Logistica buscarLogistica(String codigoLogistica)
	{
		LogisticaBean logisticaBean = manager.find(LogisticaBean.class, codigoLogistica);
		if (logisticaBean != null)
		{
			Logistica logistica = new Logistica(logisticaBean.getIdLogistica(), logisticaBean.getUrlEventos(), logisticaBean.getUrlNotificacionDespacho());
			logistica.setAddress(logisticaBean.getAddress());
			logistica.setEnabled(logisticaBean.isEnabled());
			logistica.setSync(logisticaBean.isSync());
			logistica.setDestination(logisticaBean.getDestination());
			logistica.setUsername(logisticaBean.getUsername());
			logistica.setPassword(logisticaBean.getPassword());
			return logistica;
		}
		else
			return null;
	}
	public List<Logistica> getListLogistica()
	{
		try{
			String query = "SELECT l "
					+ "FROM LogisticaBean l ";
		
			@SuppressWarnings("unchecked")
			List<LogisticaBean> listaBeans = manager.createQuery(query).getResultList();
			List<Logistica> lista = new ArrayList<Logistica>();
			if(listaBeans != null)
			{
				for (LogisticaBean bean: listaBeans)
				{
					Logistica log = new Logistica(bean.getIdLogistica(), bean.getUrlEventos(), bean.getUrlNotificacionDespacho());
					log.setAddress(bean.getAddress());
					log.setDestination(bean.getDestination());
					log.setEnabled(bean.isEnabled());
					log.setPassword(bean.getPassword());
					log.setSync(bean.isSync());
					log.setUsername(bean.getUsername());
					lista.add(log);
				}
			}
			
			return lista;
		}catch(Exception e){
			System.out.println(e);
			return null;
		}
	}
	
	public Logistica buscarLogisticaActivo()
	{
		String query = "SELECT logistica "
				+ "FROM LogisticaBean logistica "
				+ "WHERE logistica.enabled > 0"
						;
		LogisticaBean logisticaBean = (LogisticaBean) manager.createQuery(query).setMaxResults(1).getSingleResult();
		if (logisticaBean != null)
		{
			Logistica logistica = new Logistica(logisticaBean.getIdLogistica(), logisticaBean.getUrlEventos(), logisticaBean.getUrlNotificacionDespacho());
			logistica.setAddress(logisticaBean.getAddress());
			logistica.setEnabled(logisticaBean.isEnabled());
			logistica.setSync(logisticaBean.isSync());
			logistica.setDestination(logisticaBean.getDestination());
			logistica.setUsername(logisticaBean.getUsername());
			logistica.setPassword(logisticaBean.getPassword());
			return logistica;
		}
		else
			return null;
	}
	
	public void actualizarLogistica(Logistica logistica)
	{
		LogisticaBean logisticaBean = manager.find(LogisticaBean.class, logistica.getIdLogistica());
		if (logisticaBean != null)
		{
			logisticaBean.setAddress(logistica.getAddress());
			logisticaBean.setEnabled(logistica.isEnabled());
			logisticaBean.setUrlEventos(logistica.getUrlEventos());
			logisticaBean.setUrlNotificacionDespacho(logistica.getUrlNotificacionDespacho());
			logisticaBean.setSync(logistica.isSync());
			logisticaBean.setDestination(logistica.getDestination());
			logisticaBean.setUsername(logistica.getUsername());
			logisticaBean.setPassword(logistica.getPassword());
			manager.merge(logisticaBean);
		}
	}
	
	public void eliminarLogistica(Logistica logistica)
	{
		LogisticaBean logisticaBean = manager.find(LogisticaBean.class, logistica.getIdLogistica());
		if (logisticaBean != null)
			manager.remove(logisticaBean);
	}

}
