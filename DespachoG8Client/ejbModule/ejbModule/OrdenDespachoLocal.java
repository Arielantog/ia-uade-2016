package ejbModule;

import javax.ejb.Local;

import dto.OrdenDespachoDTO;

@Local
public interface OrdenDespachoLocal {
	
	public int cargarOrdenDespacho(OrdenDespachoDTO ordenDTO);

}
