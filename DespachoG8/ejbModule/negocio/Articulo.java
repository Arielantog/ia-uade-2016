package negocio;

import dto.ArticuloDTO;
import entities.ArticuloBean;

public class Articulo {
	private int codigoArticulo;
	private String descripcion;
	
	public Articulo(int codigoArticulo, String descripcion) {
		this.codigoArticulo = codigoArticulo;
		this.descripcion = descripcion;
	}
	public int getCodigoArticulo() {
		return codigoArticulo;
	}
	public void setCodigoArticulo(int codigoArticulo) {
		this.codigoArticulo = codigoArticulo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	
	public ArticuloDTO getArticuloDTO(){
		ArticuloDTO articulo = new ArticuloDTO();
		articulo.setCodigoArticulo(codigoArticulo);
		articulo.setDescripcion(descripcion);
		return articulo;
	}
	
	public ArticuloBean getBean() {
		ArticuloBean articuloBean = new ArticuloBean();
		articuloBean.setCodigoArticulo(codigoArticulo);
		articuloBean.setDescripcion(descripcion);
		return articuloBean;
	}
}
