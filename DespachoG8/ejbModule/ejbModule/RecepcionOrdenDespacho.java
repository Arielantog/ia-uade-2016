package ejbModule;

import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import dto.ItemDespachoDTO;
import dto.OrdenDespachoDTO;
import manager.ManagerPersistencia;
import negocio.Articulo;
import negocio.OrdenDespacho;
import negocio.PortalWeb;

/**
 * Session Bean implementation class OrdenDespacho
 */
@Stateless
@LocalBean
public class RecepcionOrdenDespacho implements OrdenDespachoLocal {

	@EJB
	private ManagerPersistencia manager;
	
	@EJB
	private CrearSolicitudArticulo solicitud;
	
	@EJB
	private Notificaciones notificar;
	
    /**
     * Default constructor. 
     */
    public RecepcionOrdenDespacho() {
        // TODO Auto-generated constructor stub
    }
    
    public int cargarOrdenDespacho(OrdenDespachoDTO ordenDTO)
    {
    	Logger.getAnonymousLogger().info("Entrando a Recepcion de Orden de Despacho Cargar Orden");
    	OrdenDespacho orden = new OrdenDespacho(new PortalWeb(ordenDTO.getPortalWeb().getCodigoPortalWeb()), ordenDTO.getIdVenta());
    	for(ItemDespachoDTO itemDTO: ordenDTO.getItemsDespacho())
    	{
    		Articulo art = manager.buscarArticulo(itemDTO.getArticulo().getCodigoArticulo());
    		orden.agregarItem(art, itemDTO.getCantidad());
    	}
    	int nroOrden = manager.nuevarOrdenDespacho(orden);
    	if (nroOrden != 0)
    		notificar.notificarEventoLogistica(manager.buscarLogisticaActivo(), "Orden de Despacho para Portal: " + orden.getPortalWeb().getCodigoPortalWeb() + ". Numero: " + orden.getPublicCodigoOrdenDespacho() + ".");
    	orden.setCodigoOrdenDespacho(nroOrden);
    	
    	solicitud.generarMensajesSolicitud(orden);
    	
    	Logger.getAnonymousLogger().info("Solicitudes de Articulos presumiblemente enviadas.");
    	
    	return orden.getPublicCodigoOrdenDespacho();
    }
    

}
